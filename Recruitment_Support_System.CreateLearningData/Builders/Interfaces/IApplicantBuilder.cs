﻿using Recruitment_Support_System.Models;
using Recruitment_Support_System.Models.CurriculumVitae;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment_Support_System.CreateLearningData
{
    interface IApplicantBuilder
    {
        void BuildExUser(int id);
        void BuildUserExp(List<string> exp);
        void BuildUserEdu(List<string> edu);
        void BuildUserCour(List<string> cour);
        void BuildUserIntr(List<string> intr);
        void BuildUserLang();
        void BuildUserSkill(List<string> sk);
        void BuildFullCv();

        ExtendedUser GetUser();
        CV GetCv();
    }
}
