﻿using Microsoft.AspNet.Identity.EntityFramework;
using Recruitment_Support_System.Models;
using Recruitment_Support_System.Models.Builders.CvBuilders;
using Recruitment_Support_System.Models.CurriculumVitae;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Owin;
using Recruitment_Support_System.CreateLearningData.Enums;
using Recruitment_Support_System.Models.Enums;

namespace Recruitment_Support_System.CreateLearningData
{
    class JuniorApplicantBuilder : IApplicantBuilder
    {
        private ExtendedUser user;
        private CV cV;
        private int age;
        private Random r = new Random();
        private int typeOFApplicant;
        List<string> _o;
        List<string> _c;

        private ApplicationDbContext db;
        private ApplicationDbContext Db { get => db ?? new ApplicationDbContext(); set => db = value; }
        //private EUserExp UserExp;

        public JuniorApplicantBuilder(List<string> orgs, List<string> comp)
        {
            _o = orgs; _c = comp;
        }

        public void BuildExUser(int id)
        {
            age = r.Next(18, 67);
            typeOFApplicant = r.Next(0, 3);
            user = new ExtendedUser() { Email = "JuniorTest" + id + "@Email.com", Age = age, EmailConfirmed = true, Birthdate = DateTime.Now.AddYears(-age).AddMonths(r.Next(-5, 6)).AddDays(r.Next(0, 30)), UserName = "TestUser" + id, Name = "Imie" + id, Surname = "Nazwisko" + id, PasswordHash = "AG25/qYue6BS2VQ+aHNN9WczL4FEnJ5DsUK52uiV2SNjZTBMQUACjc5LTjj2hZziSA==", SecurityStamp = "52166ffc-6bd1-4412-b0f9-e19a50fe1bf4" };
        }

        public void BuildFullCv()
        {
            cV = new CV(user);
        }

        public void BuildUserCour(List<string> cour)
        {
            var co = new List<string>();
            co.AddRange(cour);
            var num = typeOFApplicant + r.Next(-2, 2);
            for (int i = 0; i < num; i++)
            {
                var date = DateTime.Now.AddYears(-r.Next(1, age - 16));
                string c = co[r.Next(0, 12 - i)];
                co.Remove(c);
                cV.Courses.Add(new Course() { Name = c, Org = _o[r.Next(5)], Passed = date, ValidTo = date.AddYears(r.Next(3, 6)) });
            }
        }

        public void BuildUserEdu(List<string> edu)
        {
            var ed = new List<string>();
            ed.AddRange(edu);
            bool isTechnik = r.Next(2) == 0 ? false : true;
            var num = r.Next(4);
            var yearsofSchool = num * 3;
            var date = DateTime.Now.AddYears(-r.Next(yearsofSchool, age - yearsofSchool));
            var end = date.AddYears(4);
            for (int i = isTechnik ? 0 : 1; i < num; i++)
            {
                string c = ed[r.Next(0, 10 - i)];
                ed.Remove(c);
                cV.Educations.Add(new Education() { Name = c, Title = Enum.GetName(typeof(UserSchool), i), Start = date, End = end });
                date = end;
                switch (i)
                {
                    case 0:
                        end.AddYears(4);
                        break;
                    case 1:
                        end.AddYears(1);
                        break;
                    case 2:
                        end.AddYears(r.Next(2, 5));
                        break;
                }
            }
        }

        public void BuildUserExp(List<string> exp)
        {
            // jun - 0 -2 years of exp.
            var ex = new List<string>();
            ex.AddRange(exp);
            var num = typeOFApplicant;
            int yersOfExp = r.Next(0, 3);
            int period = yersOfExp / ((num == 0) ? 1 : num);
            var date = DateTime.Now.AddYears(-yersOfExp);
            for (int i = 0; i < num; i++)
            {
                var end = date.AddYears(period);
                string c = ex[r.Next(0, 5 - i)];
                ex.Remove(c);
                cV.Jobs.Add(new Job() { Comment = "TestJob" + i, Name = c, Company = _c[r.Next(10)], Start = date, End = end });
                date = end;
            }
        }

        public void BuildUserIntr(List<string> intr)
        {
            var inr = new List<string>();
            inr.AddRange(intr);
            var num = typeOFApplicant + r.Next(-2, 6);
            for (int i = 0; i < num; i++)
            {
                string c = inr[r.Next(0, 30 - i)];
                inr.Remove(c);
                cV.Intrests.Add(new Intrest() { Name = c });
            }
        }

        public void BuildUserLang()
        {
            var langDict = Db.LanguageDictionary.ToList();
            var num = typeOFApplicant + r.Next(-2, 2);
            for (int i = 0; i < num; i++)
            {
                var c = langDict[r.Next(0, 10 - i)];
                langDict.Remove(c);
                cV.Languages.Add(new Language() { LanguageDictionary = c, Level = (LanguageLevel)r.Next(6) });
            }
        }

        public void BuildUserSkill(List<string> ski)
        {
            var sk = new List<string>();
            sk.AddRange(ski);
            var num = typeOFApplicant + r.Next(-2, 2);
            for (int i = 0; i < num; i++)
            {
                string c = sk[r.Next(0, 10 - i)];
                sk.Remove(c);
                cV.Skills.Add(new Skill() { Name = c, Level = (Models.Enums.SkillLevel)r.Next(0, 3) });
            }
        }

        public ExtendedUser GetUser()
        {
            return user;
        }

        public CV GetCv()
        {
            return cV;
        }
    }
}
