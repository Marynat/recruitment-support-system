﻿using Microsoft.AspNet.Identity.EntityFramework;
using Recruitment_Support_System.Models;
using Recruitment_Support_System.Models.Builders.CvBuilders;
using Recruitment_Support_System.Models.CurriculumVitae;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Owin;
using Recruitment_Support_System.CreateLearningData.Enums;

namespace Recruitment_Support_System.CreateLearningData
{
    class CompanyBuilder : IApplicantBuilder
    {
        private ExtendedUser user;
        private CV cV;
        private int age;
        private Random r = new Random();
        private int typeOFApplicant;
        List<string> _o;
        List<string> _c;
        //private EUserExp UserExp;

        public void BuildExUser(int id)
        {
            age = r.Next(18, 67);
            typeOFApplicant = r.Next(5, 8);
            user = new ExtendedUser() { Email = "TestCompany" + id + "@Email.com", Age = age, EmailConfirmed = true, Birthdate = DateTime.Now.AddYears(-age).AddMonths(r.Next(-5, 6)).AddDays(r.Next(0, 30)), UserName = "TestCompany" + id, Name = "Imie" + id, Surname = "Nazwisko" + id, PasswordHash = "AG25/qYue6BS2VQ+aHNN9WczL4FEnJ5DsUK52uiV2SNjZTBMQUACjc5LTjj2hZziSA==", SecurityStamp = "52166ffc-6bd1-4412-b0f9-e19a50fe1bf4" };
            //if (age < 25)
            //{
            //    UserExp = EUserExp.junior;
            //}
            //else if (age < 34)
            //{
            //    UserExp = EUserExp.mid;
            //}
            //else if (age < 45)
            //{
            //    UserExp = EUserExp.exp;
            //}
            //else
            //{
            //    UserExp = EUserExp.pro;
            //}
        }

        public void BuildFullCv()
        {
            cV = new CV(user);
        }

        public void BuildUserCour(List<string> cour)
        {
            throw new NotImplementedException();
        }

        public void BuildUserEdu(List<string> edu)
        {
            throw new NotImplementedException();
        }

        public void BuildUserExp(List<string> exp)
        {
            throw new NotImplementedException();
        }

        public void BuildUserIntr(List<string> intr)
        {
            throw new NotImplementedException();
        }

        public void BuildUserLang()
        {
            throw new NotImplementedException();
        }

        public void BuildUserSkill(List<string> sk)
        {
            throw new NotImplementedException();
        }

        public CV GetCv()
        {
            return cV;
        }

        public ExtendedUser GetUser()
        {
            throw new NotImplementedException();
        }
    }
}
