﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Recruitment_Support_System.CreateLearningData.Directors;
using Recruitment_Support_System.CreateLearningData.Enums;
using Recruitment_Support_System.Models;
using Recruitment_Support_System.Models.CurriculumVitae;
using Recruitment_Support_System.Models.Dictionaries;
using Recruitment_Support_System.Models.Enums;

namespace Recruitment_Support_System.CreateLearningData
{
    class Program
    {
        private static List<CV> cvs = new List<CV>();
        private static List<JobOffer> jobOffers = new List<JobOffer>();

        private static ApplicationDbContext db = new ApplicationDbContext();
        private static Random r = new Random();
        static void Main(string[] args)
        {
            createUsers();
            createJobOffers();
            createApplicants();
            updateDatabase();
        }

        public static List<string> PhpSkills = new List<string>();
        public static List<string> PhpExp = new List<string>();
        public static List<string> PhpEd = new List<string>();
        public static List<string> PhpCour = new List<string>();
        public static List<string> PhpInt = new List<string>();
        public static List<string> CSharpSkills = new List<string>();
        public static List<string> CSharpExp = new List<string>();
        public static List<string> CSharpEd = new List<string>();
        public static List<string> CSharpCour = new List<string>();
        public static List<string> CSharpInt = new List<string>();
        public static List<string> JavaSkills = new List<string>();
        public static List<string> JavaExp = new List<string>();
        public static List<string> JavaEd = new List<string>();
        public static List<string> JavaCour = new List<string>();
        public static List<string> JavaInt = new List<string>();

        private static List<string> orgs = new List<string>();
        private static List<string> companies = new List<string>();

        private static void SetSkills()
        {
            CSharpSkills.AddRange(new string[] { "angular", "c#", ".net", "git", "sql", "testy jednostkowe", "ci/cd", "mssql", "entity framework", "cloud" });
            JavaSkills.AddRange(new string[] { "git", "sql", "cloud", "testy jednostkowe", "ci/cd", "java script", "hibernate", "maven", "spring", "gradle" });
            PhpSkills.AddRange(new string[] { "git", "sql", "cloud", "testy jednostkowe", "ci/cd", "symfony", "twig", "composer", "ajax", "jquery" });
        }
        private static void SetExp()
        {
            CSharpExp.AddRange(new string[] { "młodszy programista .net", "programista c#", "programista .net", "starszy programista .net", "programista baz danych" });
            JavaExp.AddRange(new string[] { "programista fullstack", "młodszy programista java", "programista java", "starszy programista java", "tester" });
            PhpExp.AddRange(new string[] { "programista php", "młodszy programista php", "programista php - frontend", "starszy programista", "programista fullstack" });
        }
        private static void SetEd()
        {
            var p = new string[] { "informatyka", "psychologia i informatyka", "automatyka i informatyka przemysłowa", "automatyka", "cyberbezpieczeństwo", "ui/ux designer", "grafika komputerowa", "inżynieria danych", "teleinformatyka", "informatyka algorytmiczna" };
            CSharpEd.AddRange(p);
            JavaEd.AddRange(p);
            PhpEd.AddRange(p);
        }
        private static void SetCour()
        {
            CSharpCour.AddRange(new string[] { ".net (c#) developer", "programowanie .net – kurs podstawowy", ".net developer", "c# .net od podstaw", "weekendowy bootcamp c#/.net", "nauka programowania w c# i technologii .net", "przyjdź na bootcamp i naucz się programować w c# i .net", "kurs języka c# 5.0", "zostań programistą asp.net", "kursdotnet.pl", "c# od podstaw 2020", "c# zaawansowany - programuj w c# na zawodowym poziomie" });
            JavaCour.AddRange(new string[] { "JAVA DEVELOPER", "Kurs Programowania Java od Podstaw", "Java od podstaw – kurs zdalny", "Kurs programowania Java", "Kurs Java: Nauka jednego z najpopularniejszych języków programowania", "Nauka programowania i język Java", "KURS JAVA PODSTAWY", "Poznaj Javę", "Kurs Java", "Java od Podstaw do Eksperta - twórz własne aplikacje", "Java - ambitny start.", "Fundamenty języka Java", "Praktyczny kurs Java", "Programowanie w Javie: solidne fundamenty" });
            PhpCour.AddRange(new string[] { "programista php", "kurs php", "web developer: php + javascript", "html, css oraz wstęp do javascript i php - poziom zaawansowany - tworzenie statycznych stron internetowych", "php - poziom podstawowy - tworzenie dynamicznych stron internetowych", "php - poziom zaawansowany - projektowanie aplikacji internetowych", "kurs php 7", "programowanie w php 7.4 od podstaw - teoria i praktyka", "php & mysql od podstaw do eksperta", "php (back-end)", "co nowego w php 7", "kurs php 8" });
        }
        private static void SetInt()
        {
            var p = new string[] { "samochody", "gry komputerowe", "gotowanie", "Piłka nożna", "Muzyka", "Własne projekty", "Czytanie", "Jazda na nartach", "Trekking", "Gry planszowe", "Gym", "Łyżwiarstwo", "Bieganie", "Majsterkowanie", "natura", "joga", "malastwo", "nurkowanie", "pływanie", "podróżowanie", "sztuka", "taniec", "szermierka", "szachy", "kajaki", "kręgle", "książki", "boks", "kolekcjonowanie", "bilard" };
            CSharpInt.AddRange(p);
            JavaInt.AddRange(p);
            PhpInt.AddRange(p);
        }

        private static void createUsers()
        {
            SetCour();
            SetEd();
            SetExp();
            SetInt();
            SetSkills();

            orgs.AddRange(new string[] { "coders lab", "udemy", "sdacademy", "kodilla", "samouczekprogramisty" });
            companies.AddRange(new string[] { "gogle", "amzon", "pear", "lbit", "itenter", "telest", "pwż", "enBank", "srkom", "previt" });

            List<IApplicantBuilder> builders = new List<IApplicantBuilder>();
            builders.Add(new ProAppBuilder(orgs, companies));
            builders.Add(new JuniorApplicantBuilder(orgs, companies));
            builders.Add(new ExpAppBuilder(orgs, companies));
            builders.Add(new MidAppBuilder(orgs, companies));
            AppDirector director = new AppDirector();
            for (int i = 1; i < 334;)
            {
                director.setBuilder(builders[(i % 4)]);
                director.BuildFullCSharpCV(i++);
                cvs.Add(director.GetCV());
                director.BuildFullJavaCV(i++);
                cvs.Add(director.GetCV());
                director.BuildFullPhpCV(i++);
                cvs.Add(director.GetCV());
            }
        }

        private static void createJobOffers()
        {
            var comp = db.Companies.ToList();
            for (int i = 0; i < 30; i++)
            {
                var joblvl = (JobLevel)r.Next(5);
                jobOffers.Add(new JobOffer() { About = "Praca programisty " + Enum.GetName(typeof(ProgrammingCategory), i % 3), Company = comp[i % 10], Currency = "PLN", ExpirationDate = DateTime.Now.AddMonths(3), IsDeleted = false, JobContract = JobContract.UOP, JobLevel = joblvl, OkresRozliczeniowy = billingPeriod.miesięczny, Remote = r.Next(2) > 0 ? true : false, RemoteInterview = true, ZarobkiMin = r.Next(1000 * (((int)joblvl + 1) * 3)), ZarobkiMax = r.Next(2000 * (((int)joblvl + 1) * 3)), Title = "Programista " + Enum.GetName(typeof(ProgrammingCategory), i % 3), TeamSize = (short)r.Next(20, 200), Applications = new List<Application>() });  //think how to solve skills and requirements
            }
        }

        private static void createApplicants()
        {
            var userRole = db.Roles.FirstOrDefault(r => r.Name == "User");
            foreach (var c in cvs)
            {
                var ur = new IdentityUserRole() { UserId = c.User.Id, RoleId = userRole.Id };
                c.User.Roles.Add(ur);
                int curRand = r.Next(30);
                jobOffers[curRand].Applications.Add(new Application() { ExtendedUser_Id = c.User.Id, JobOffer_Id = jobOffers[curRand].Id });
            }
        }

        private static void updateDatabase()
        {
            foreach (var c in cvs)
                db.CVs.Add(c);

            db.JobOffers.AddRange(jobOffers);
            db.SaveChanges();
        }

    }
}
