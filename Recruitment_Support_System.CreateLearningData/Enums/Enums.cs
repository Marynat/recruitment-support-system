﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment_Support_System.CreateLearningData.Enums
{
    enum EUserExp
    {
        junior,
        mid,
        exp,
        pro
    }
    enum UserSchool
    {
        Technik,
        Inżynier,
        Magister,
        Dr
    }
    enum ProgrammingCategory
    {
        Php,
        CSharp,
        Java
    }
}
