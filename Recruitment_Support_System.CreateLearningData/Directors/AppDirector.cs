﻿using Recruitment_Support_System.Models;
using Recruitment_Support_System.Models.CurriculumVitae;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment_Support_System.CreateLearningData.Directors
{
    class AppDirector
    {
        private IApplicantBuilder appBuilder;

        public AppDirector() { }
        public AppDirector(IApplicantBuilder builder)
        {
            appBuilder = builder;
        }

        //public void BuildBasicCV(CV cV)
        //{
        //    appBuilder.BuildEducation(cV.Educations);
        //    appBuilder.BuildJobs(cV.Jobs);
        //    appBuilder.BuildLanguages(cV.Languages);
        //    appBuilder.BuildSkills(cV.Skills);
        //}

        public void setBuilder(IApplicantBuilder builder)
        {
            appBuilder = builder;
        }

        public void BuildFullPhpCV(int i)
        {
            appBuilder.BuildExUser(i);
            appBuilder.BuildFullCv();
            appBuilder.BuildUserCour(Program.PhpCour);
            appBuilder.BuildUserEdu(Program.PhpEd);
            appBuilder.BuildUserExp(Program.PhpExp);
            appBuilder.BuildUserIntr(Program.PhpInt);
            appBuilder.BuildUserLang();
            appBuilder.BuildUserSkill(Program.PhpSkills);
        }

        public void BuildFullCSharpCV(int i)
        {
            appBuilder.BuildExUser(i);
            appBuilder.BuildFullCv();
            appBuilder.BuildUserCour(Program.CSharpCour);
            appBuilder.BuildUserEdu(Program.CSharpEd);
            appBuilder.BuildUserExp(Program.CSharpExp);
            appBuilder.BuildUserIntr(Program.CSharpInt);
            appBuilder.BuildUserLang();
            appBuilder.BuildUserSkill(Program.CSharpSkills);
        }
        public void BuildFullJavaCV(int i)
        {
            appBuilder.BuildExUser(i);
            appBuilder.BuildFullCv();
            appBuilder.BuildUserCour(Program.JavaCour);
            appBuilder.BuildUserEdu(Program.JavaEd);
            appBuilder.BuildUserExp(Program.JavaExp);
            appBuilder.BuildUserIntr(Program.JavaInt);
            appBuilder.BuildUserLang();
            appBuilder.BuildUserSkill(Program.JavaSkills);
        }

        public ExtendedUser getUser()
        {
            return appBuilder.GetUser();
        }

        public CV GetCV()
        {
            return appBuilder.GetCv();
        }
    }
}
