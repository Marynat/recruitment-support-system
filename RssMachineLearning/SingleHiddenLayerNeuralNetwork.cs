﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RssMachineLearning
{

    public class SingleHiddenLayerNeuralNetwork
    {
        private static SingleHiddenLayerNeuralNetwork _instance;

        private int numIn;
        private int numHidd;
        private int numOut;

        private double[] inp;
        private double[][] ihW;
        private double[] hBi;
        private double[] hOut;

        private double[][] hoW;
        private double[] oBi;
        private double[] outp;

        private Random rnd;

        public static SingleHiddenLayerNeuralNetwork GetInststance()
        {
            if (_instance == null)
                _instance = new SingleHiddenLayerNeuralNetwork(4, 5, 3);
            return _instance;
        }

        public SingleHiddenLayerNeuralNetwork(int numInput, int numHidden, int numOutput)
        {
            this.numIn = numInput;
            this.numHidd = numHidden;
            this.numOut = numOutput;

            this.inp = new double[numInput];

            this.ihW = MakeMatrix(numInput, numHidden, 0.0);
            this.hBi = new double[numHidden];
            this.hOut = new double[numHidden];

            this.hoW = MakeMatrix(numHidden, numOutput, 0.0);
            this.oBi = new double[numOutput];
            this.outp = new double[numOutput];

            this.rnd = new Random(0);
            this.InitializeWeights();
            _instance = this;
        }

        private static double[][] MakeMatrix(int rows,
          int cols, double v)
        {
            double[][] result = new double[rows][];
            for (int r = 0; r < result.Length; ++r)
                result[r] = new double[cols];
            for (int i = 0; i < rows; ++i)
                for (int j = 0; j < cols; ++j)
                    result[i][j] = v;
            return result;
        }

        private void InitializeWeights()
        {
            int numWeights = (numIn * numHidd) +
              (numHidd * numOut) + numHidd + numOut;
            double[] initialWeights = new double[numWeights];
            for (int i = 0; i < initialWeights.Length; ++i)
                initialWeights[i] = (0.001 - 0.0001) * rnd.NextDouble() + 0.0001;
            this.SetWeights(initialWeights);
        }

        public void SetWeights(double[] weights)
        {

            int numWeights = (numIn * numHidd) +
              (numHidd * numOut) + numHidd + numOut;
            if (weights.Length != numWeights)
                throw new Exception("Bad weights array in SetWeights");

            int k = 0;

            for (int i = 0; i < numIn; ++i)
                for (int j = 0; j < numHidd; ++j)
                    ihW[i][j] = weights[k++];
            for (int i = 0; i < numHidd; ++i)
                hBi[i] = weights[k++];
            for (int i = 0; i < numHidd; ++i)
                for (int j = 0; j < numOut; ++j)
                    hoW[i][j] = weights[k++];
            for (int i = 0; i < numOut; ++i)
                oBi[i] = weights[k++];
        }

        public double[] GetWeights()
        {
            int numWeights = (numIn * numHidd) +
              (numHidd * numOut) + numHidd + numOut;
            double[] result = new double[numWeights];
            int k = 0;
            for (int i = 0; i < ihW.Length; ++i)
                for (int j = 0; j < ihW[0].Length; ++j)
                    result[k++] = ihW[i][j];
            for (int i = 0; i < hBi.Length; ++i)
                result[k++] = hBi[i];
            for (int i = 0; i < hoW.Length; ++i)
                for (int j = 0; j < hoW[0].Length; ++j)
                    result[k++] = hoW[i][j];
            for (int i = 0; i < oBi.Length; ++i)
                result[k++] = oBi[i];
            return result;
        }

        public double[] GenerateOutputs(double[] xValues)
        {
            double[] hSums = new double[numHidd];
            double[] oSums = new double[numOut];

            for (int i = 0; i < xValues.Length; ++i)
                this.inp[i] = xValues[i];

            for (int j = 0; j < numHidd; ++j)
                for (int i = 0; i < numIn; ++i)
                    hSums[j] += this.inp[i] * this.ihW[i][j];

            for (int i = 0; i < numHidd; ++i)
                hSums[i] += this.hBi[i];

            for (int i = 0; i < numHidd; ++i)
                this.hOut[i] = HyperTan(hSums[i]);

            for (int j = 0; j < numOut; ++j)
                for (int i = 0; i < numHidd; ++i)
                    oSums[j] += hOut[i] * hoW[i][j];

            for (int i = 0; i < numOut; ++i)
                oSums[i] += oBi[i];

            double[] softOut = Softmax(oSums);
            Array.Copy(softOut, outp, softOut.Length);

            double[] retResult = new double[numOut];
            Array.Copy(this.outp, retResult, retResult.Length);
            return retResult;
        }

        private static double HyperTan(double x)
        {
            if (x < -20.0) return -1.0;
            else if (x > 20.0) return 1.0;
            else return Math.Tanh(x);
        }

        private static double[] Softmax(double[] oSums)
        {

            double sum = 0.0;
            for (int i = 0; i < oSums.Length; ++i)
                sum += Math.Exp(oSums[i]);

            double[] result = new double[oSums.Length];
            for (int i = 0; i < oSums.Length; ++i)
                result[i] = Math.Exp(oSums[i]) / sum;

            return result;
        }

        public double[] Train(double[][] trainData, int maxEpochs, double learnRate, double momentum)
        {

            double[][] hoGrads = MakeMatrix(numHidd, numOut, 0.0);
            double[] obGrads = new double[numOut];

            double[][] ihGrads = MakeMatrix(numIn, numHidd, 0.0);
            double[] hbGrads = new double[numHidd];

            double[] oSignals = new double[numOut];
            double[] hSignals = new double[numHidd];

            double[][] ihPrevWeightsDelta = MakeMatrix(numIn, numHidd, 0.0);
            double[] hPrevBiasesDelta = new double[numHidd];
            double[][] hoPrevWeightsDelta = MakeMatrix(numHidd, numOut, 0.0);
            double[] oPrevBiasesDelta = new double[numOut];

            int epoch = 0;
            double[] xValues = new double[numIn];
            double[] tValues = new double[numOut];
            double derivative = 0.0;
            double errorSignal = 0.0;

            int[] sequence = new int[trainData.Length];
            for (int i = 0; i < sequence.Length; ++i)
                sequence[i] = i;

            int errInterval = maxEpochs / 20;
            while (epoch < maxEpochs)
            {
                ++epoch;

if (epoch % errInterval == 0 && epoch < maxEpochs)
{
    double trainErr = Error(trainData);
    Debug.WriteLine("epoch = " + epoch + "  error = " +
      trainErr.ToString("F4"));
}

Shuffle(sequence);
                for (int ii = 0; ii < trainData.Length; ++ii)
                {
                    int idx = sequence[ii];
                    Array.Copy(trainData[idx], xValues, numIn);
                    Array.Copy(trainData[idx], numIn, tValues, 0, numOut);
                    GenerateOutputs(xValues);


                    for (int k = 0; k < numOut; ++k)
                    {
                        errorSignal = tValues[k] - outp[k];
                        derivative = (1 - outp[k]) * outp[k];
                        oSignals[k] = errorSignal * derivative;
                    }

                    for (int j = 0; j < numHidd; ++j)
                        for (int k = 0; k < numOut; ++k)
                            hoGrads[j][k] = oSignals[k] * hOut[j];

                    for (int k = 0; k < numOut; ++k)
                        obGrads[k] = oSignals[k] * 1.0;

                    for (int j = 0; j < numHidd; ++j)
                    {
                        derivative = (1 + hOut[j]) * (1 - hOut[j]);
                        double sum = 0.0;
                        for (int k = 0; k < numOut; ++k)
                        {
                            sum += oSignals[k] * hoW[j][k];
                        }
                        hSignals[j] = derivative * sum;
                    }

                    for (int i = 0; i < numIn; ++i)
                        for (int j = 0; j < numHidd; ++j)
                            ihGrads[i][j] = hSignals[j] * inp[i];

                    for (int j = 0; j < numHidd; ++j)
                        hbGrads[j] = hSignals[j] * 1.0;



                    for (int i = 0; i < numIn; ++i)
                    {
                        for (int j = 0; j < numHidd; ++j)
                        {
                            double delta = ihGrads[i][j] * learnRate;
                            ihW[i][j] += delta;
                            ihW[i][j] += ihPrevWeightsDelta[i][j] * momentum;
                            ihPrevWeightsDelta[i][j] = delta;
                        }
                    }

                    for (int j = 0; j < numHidd; ++j)
                    {
                        double delta = hbGrads[j] * learnRate;
                        hBi[j] += delta;
                        hBi[j] += hPrevBiasesDelta[j] * momentum;
                        hPrevBiasesDelta[j] = delta;
                    }

                    for (int j = 0; j < numHidd; ++j)
                    {
                        for (int k = 0; k < numOut; ++k)
                        {
                            double delta = hoGrads[j][k] * learnRate;
                            hoW[j][k] += delta;
                            hoW[j][k] += hoPrevWeightsDelta[j][k] * momentum;
                            hoPrevWeightsDelta[j][k] = delta;
                        }
                    }

                    for (int k = 0; k < numOut; ++k)
                    {
                        double delta = obGrads[k] * learnRate;
                        oBi[k] += delta;
                        oBi[k] += oPrevBiasesDelta[k] * momentum;
                        oPrevBiasesDelta[k] = delta;
                    }

                }

            }
            double[] bestWts = GetWeights();
            SetWeights(bestWts);
            return bestWts;
        }

        private void Shuffle(int[] sequence)
        {
            for (int i = 0; i < sequence.Length; ++i)
            {
                int r = this.rnd.Next(i, sequence.Length);
                int tmp = sequence[r];
                sequence[r] = sequence[i];
                sequence[i] = tmp;
            }
        }

        private double Error(double[][] trainData)
        {
            double sumSquaredError = 0.0;
            double[] xValues = new double[numIn];
            double[] tValues = new double[numOut];

            for (int i = 0; i < trainData.Length; ++i)
            {
                Array.Copy(trainData[i], xValues, numIn);
                Array.Copy(trainData[i], numIn, tValues, 0, numOut);
                double[] yValues = this.GenerateOutputs(xValues);
                for (int j = 0; j < numOut; ++j)
                {
                    double err = tValues[j] - yValues[j];
                    sumSquaredError += err * err;
                }
            }
            return sumSquaredError / trainData.Length;
        }

        public double Accuracy(double[][] testData)
        {
            int numCorrect = 0;
            int numWrong = 0;
            double[] xValues = new double[numIn];
            double[] tValues = new double[numOut];
            double[] yValues;

            for (int i = 0; i < testData.Length; ++i)
            {
                Array.Copy(testData[i], xValues, numIn);
                Array.Copy(testData[i], numIn, tValues, 0, numOut);
                yValues = this.GenerateOutputs(xValues);
                int maxIndex = MaxIndex(yValues);
                int tMaxIndex = MaxIndex(tValues);

                if (maxIndex == tMaxIndex)
                    ++numCorrect;
                else
                    ++numWrong;
            }
            return (numCorrect * 1.0) / (numCorrect + numWrong);
        }

        private static int MaxIndex(double[] vector)
        {
            int bigIndex = 0;
            double biggestVal = vector[0];
            for (int i = 0; i < vector.Length; ++i)
            {
                if (vector[i] > biggestVal)
                {
                    biggestVal = vector[i];
                    bigIndex = i;
                }
            }
            return bigIndex;
        }


    }
}
