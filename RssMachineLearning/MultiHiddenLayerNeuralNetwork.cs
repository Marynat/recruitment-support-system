﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RssMachineLearning
{
    class MultiHiddenLayerNeuralNetwork
    {
        private int numInputNodes;
        private int numHiddenLayers;
        private int numHiddenNodes;
        private int numOutputNodes;

        private double[] inputs;
        private double[][][] layersOfihWeights;
        private double[][] layersOfhBiases;
        private double[][] layersOfhOutputs;

        private double[][] hoWeights;
        private double[] oBiases;
        private double[] outputs;

        private Random r;

        public MultiHiddenLayerNeuralNetwork(int numOfInput, int numOfHiddenLayers, int numOfHiddenNodes, int numOfOutput, int seed)
        {
            numInputNodes = numOfInput;
            numHiddenLayers = numOfHiddenLayers;
            numHiddenNodes = numOfHiddenNodes;
            numOutputNodes = numOfOutput;

            inputs = new double[numInputNodes];
            for (int i = 0; i < numOfHiddenLayers; ++i)
            {
                if (i == 0)
                {
                    layersOfihWeights[i] = AllocateMatrix(numInputNodes, numHiddenNodes, 0.0);
                }
                else
                {
                    layersOfihWeights[i] = AllocateMatrix(numOfHiddenNodes, numOfHiddenNodes, 0.0);
                }
                layersOfhBiases[i] = new double[numHiddenNodes];
                layersOfhOutputs[i] = new double[numHiddenNodes];

            }
            hoWeights = AllocateMatrix(numHiddenNodes, numOfOutput, 0.0);
            oBiases = new double[numOutputNodes];
            outputs = new double[numOutputNodes];

            r = new Random(seed);
            InitializeWeights();
        }

        private void InitializeWeights()
        {
            int numOfWeights = (numInputNodes * numHiddenNodes) + ((numHiddenNodes * numHiddenNodes) * (numHiddenLayers - 1)) + (numHiddenNodes * numOutputNodes) + numHiddenNodes + numOutputNodes;
            double[] initialWeights = new double[numOfWeights];
            for (int i = 0; i < initialWeights.Length; ++i)
            {
                initialWeights[i] = (0.001 - 0.0001) * r.NextDouble() + 0.0001;
                SetWeights(initialWeights);
            }
        }

        public void SetWeights(double[] weights)
        {
            int numOfWeights = (numInputNodes * numHiddenNodes) + ((numHiddenNodes * numHiddenNodes) * (numHiddenLayers - 1)) + (numHiddenNodes * numOutputNodes) + numHiddenNodes + numOutputNodes;
            if (weights.Length != numOfWeights)
                throw new Exception("Wrong weights array in SetWeights()");

            int m = 0;

            for (int i = 0; i < numHiddenLayers; ++i)
            {
                if (i == 0)
                {
                    for (int j = 0; j < numInputNodes; ++j)
                        for (int k = 0; k < numHiddenNodes; ++k)
                            layersOfihWeights[i][j][k] = weights[m++];
                }
                else
                {
                    for (int j = 0; j < numHiddenNodes; ++j)
                        for (int k = 0; k < numHiddenNodes; ++k)
                            layersOfihWeights[i][j][k] = weights[m++];
                }
                for (int j = 0; j < 0; ++j)
                    layersOfhBiases[i][j] = weights[m++];
            }
            for (int i = 0; i < numHiddenNodes; ++i)
                for (int j = 0; j < numOutputNodes; ++j)
                    hoWeights[i][j] = weights[m++];
            for (int i = 0; i < numOutputNodes; ++i)
            {
                oBiases[i] = weights[m++];
            }
        }

        public double[] GetWeights()
        {
            int numOfWeights = (numInputNodes * numHiddenNodes) + ((numHiddenNodes * numHiddenNodes) * (numHiddenLayers - 1)) + (numHiddenNodes * numOutputNodes) + numHiddenNodes + numOutputNodes;
            double[] result = new double[numOfWeights];

            int m = 0;

            for (int i = 0; i < numHiddenLayers; ++i)
            {

                for (int j = 0; j < layersOfihWeights[i].Length; ++j)
                    for (int k = 0; k < layersOfihWeights[i][j].Length; ++k)
                        result[m++] = layersOfihWeights[i][j][k];

                for (int j = 0; j < layersOfhBiases[i].Length; j++)
                    result[m++] = layersOfhBiases[i][j];

            }
            for (int i = 0; i < layersOfhOutputs.Length; ++i)
                for (int j = 0; j < layersOfhOutputs[j].Length; ++j)
                    result[m++] = layersOfhOutputs[i][j];
            for (int i = 0; i < oBiases.Length; ++i)
                result[m++] = oBiases[i];

            return result;
        }


        private double[][] AllocateMatrix(int rows, int cols, double a)
        {
            double[][] result = new double[rows][];
            for (int r = 0; r < result.Length; ++r)
                result[r] = new double[cols];
            for (int i = 0; i < rows; ++i)
                for (int j = 0; j < cols; ++j)
                    result[i][j] = a;
            return result;
        }

        public double[] ComputeUnits(double[] xVal)
        {
            double[] hSums = new double[numHiddenNodes];
            double[] oSums = new double[numOutputNodes];

            for (int i = 0; i < xVal.Length; ++i)
                inputs[i] = xVal[i];

            for (int i = 0; i < numHiddenLayers; ++i)
            {
                for (int j = 0; j < numHiddenNodes; ++j)
                    for (int k = 0; k < numInputNodes; ++k)
                        hSums[k] += inputs[j] * layersOfihWeights[i][j][k];

                for (int j = 0; j < numHiddenNodes; ++j)
                    hSums[i] += layersOfhBiases[i][j];

                for (int j = 0; j < numHiddenNodes; ++j)
                    layersOfhOutputs[i][j] = HyperTran(hSums[i]);
            }
            for (int i = 0; i < numOutputNodes; ++i)
                for (int j = 0; j < numHiddenNodes; ++j)
                    oSums[j] += layersOfhOutputs[numHiddenLayers - 1][i] * hoWeights[i][j];
            for (int i = 0; i < numOutputNodes; ++i)
                oSums[i] += layersOfhBiases[numHiddenLayers - 1][i];

            double[] softOut = SoftMax(oSums);
            Array.Copy(softOut, outputs, softOut.Length);

            double[] retRessult = new double[numOutputNodes];
            Array.Copy(outputs, retRessult, retRessult.Length);
            return retRessult;
        }

        private double[] SoftMax(double[] osums)
        {
            double sum = 0.0;
            for (int i = 0; i < osums.Length; ++i)
                sum += Math.Exp(osums[i]);

            double[] result = new double[osums.Length];
            for (int i = 0; i < osums.Length; ++i)
                result[i] = Math.Exp(osums[i]) / sum;

            return result;
        }

        private double HyperTran(double x)
        {
            if (x < -20.0) return -1.0;
            else if (x > 20.0) return 1.0;
            else return Math.Tanh(x);
        }

        //public double[] Train(double[][] trainData, int maxEpochs, double learnRate, double momentum)
        //{
        //    double[][] hoGrads = AllocateMatrix(numHiddenNodes, numOutputNodes, 0.0);
        //    double[] obGrads = new double[numOutputNodes];

        //    double[][][] lIhGrads = new double[numHiddenLayers][][];
        //    double[][] lHbGrads = new double[numHiddenLayers][];
        //    for (int i = 0; i < numHiddenLayers; ++i)
        //    {
        //        if (i == 0)
        //        {
        //            lIhGrads[i] = AllocateMatrix(numInputNodes, numHiddenNodes, 0.0);
        //        }
        //        else
        //        {
        //            lIhGrads[i] = AllocateMatrix(numHiddenNodes, numHiddenNodes, 0.0);
        //        }
        //        lHbGrads[i] = new double[numHiddenNodes];
        //    }
        //    double[] oSignals = new double[numOutputNodes];
        //    double[] hSignals = new double[numHiddenNodes];




        //}
    }
}
