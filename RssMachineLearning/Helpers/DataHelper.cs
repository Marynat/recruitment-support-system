﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RssMachineLearning.SingleHiddenLayerNeuralNetwork;

namespace RssMachineLearning.Helpers
{
    public static class DataHelper
    {
        public static void ShowMatrix(double[][] matrix, int numRows, int decimals, bool indices)
        {
            int len = matrix.Length.ToString().Length;
            for (int i = 0; i < numRows; ++i)
            {
                if (indices == true)
                    Debug.Write("[" + i.ToString().PadLeft(len) + "]  ");
                for (int j = 0; j < matrix[i].Length; ++j)
                {
                    double v = matrix[i][j];
                    if (v >= 0.0)
                        Debug.Write(" ");
                    Debug.Write(v.ToString("F" + decimals) + "  ");
                }
                Debug.WriteLine("");
            }

            if (numRows < matrix.Length)
            {
                Debug.WriteLine(". . .");
                int lastRow = matrix.Length - 1;
                if (indices == true)
                    Debug.Write("[" + lastRow.ToString().PadLeft(len) + "]  ");
                for (int j = 0; j < matrix[lastRow].Length; ++j)
                {
                    double v = matrix[lastRow][j];
                    if (v >= 0.0)
                        Debug.Write(" ");
                    Debug.Write(v.ToString("F" + decimals) + "  ");
                }
            }
            Debug.WriteLine("\n");
        }

        public static void ShowVector(double[] vector, int decimals, int lineLen, bool newLine)
        {
            for (int i = 0; i < vector.Length; ++i)
            {
                if (i > 0 && i % lineLen == 0) Debug.WriteLine("");
                if (vector[i] >= 0) Debug.Write(" ");
                Debug.Write(vector[i].ToString("F" + decimals) + " ");
            }
            if (newLine == true)
                Debug.WriteLine("");
        }

        public static double[][] MakeAllData(int numInput, int numHidden, int numOutput, int numRows, int seed)
        {
            Random rnd = new Random(seed);
            int numWeights = (numInput * numHidden) + numHidden +
              (numHidden * numOutput) + numOutput;
            double[] weights = new double[numWeights];
            for (int i = 0; i < numWeights; ++i)
                weights[i] = 20.0 * rnd.NextDouble() - 10.0;

            Debug.WriteLine("Generating weights and biases:");
            ShowVector(weights, 2, 10, true);

            double[][] result = new double[numRows][];
            for (int i = 0; i < numRows; ++i)
                result[i] = new double[numInput + numOutput];

            SingleHiddenLayerNeuralNetwork gnn =
              new SingleHiddenLayerNeuralNetwork(numInput, numHidden, numOutput);
            gnn.SetWeights(weights);

            for (int r = 0; r < numRows; ++r)
            {
                double[] inputs = new double[numInput];
                for (int i = 0; i < numInput; ++i)
                    inputs[i] = 20.0 * rnd.NextDouble() - 10.0;

                double[] outputs = gnn.GenerateOutputs(inputs);

                double[] oneOfN = new double[numOutput];

                int maxIndex = 0;
                double maxValue = outputs[0];
                for (int i = 0; i < numOutput; ++i)
                {
                    if (outputs[i] > maxValue)
                    {
                        maxIndex = i;
                        maxValue = outputs[i];
                    }
                }
                oneOfN[maxIndex] = 1.0;

                int c = 0;
                for (int i = 0; i < numInput; ++i)
                    result[r][c++] = inputs[i];
                for (int i = 0; i < numOutput; ++i)
                    result[r][c++] = oneOfN[i];
            }
            return result;
        }

        public static void SplitTrainTest(double[][] allData, double trainPct, int seed, out double[][] trainData, out double[][] testData)
        {
            Random rnd = new Random(seed);
            int totRows = allData.Length;
            int numTrainRows = (int)(totRows * trainPct);
            int numTestRows = totRows - numTrainRows;
            trainData = new double[numTrainRows][];
            testData = new double[numTestRows][];

            double[][] copy = new double[allData.Length][];
            for (int i = 0; i < copy.Length; ++i)
                copy[i] = allData[i];

            for (int i = 0; i < copy.Length; ++i)
            {
                int r = rnd.Next(i, copy.Length);
                double[] tmp = copy[r];
                copy[r] = copy[i];
                copy[i] = tmp;
            }
            for (int i = 0; i < numTrainRows; ++i)
                trainData[i] = copy[i];

            for (int i = 0; i < numTestRows; ++i)
                testData[i] = copy[i + numTrainRows];
        }
    }
}
