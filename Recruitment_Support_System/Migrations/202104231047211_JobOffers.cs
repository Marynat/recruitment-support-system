namespace Recruitment_Support_System.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class JobOffers : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Languages", "Level_Id", "dbo.LanguageLevels");
            DropIndex("dbo.Languages", new[] { "Level_Id" });
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        City = c.String(),
                        Address = c.String(),
                        Size = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.JobOffers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        About = c.String(),
                        Responsibilities = c.String(),
                        Requirements = c.String(),
                        TeamSize = c.Short(nullable: false),
                        JobLevel = c.Int(nullable: false),
                        JobContract = c.Int(nullable: false),
                        Company_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.Company_Id)
                .Index(t => t.Company_Id);
            
            AddColumn("dbo.Languages", "Level", c => c.Int(nullable: false));
            AddColumn("dbo.Skills", "JobOffer_Id", c => c.Int());
            AlterColumn("dbo.Skills", "Level", c => c.Int(nullable: false));
            CreateIndex("dbo.Skills", "JobOffer_Id");
            AddForeignKey("dbo.Skills", "JobOffer_Id", "dbo.JobOffers", "Id");
            DropColumn("dbo.Languages", "Level_Id");
            DropTable("dbo.LanguageLevels");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.LanguageLevels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Level = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Languages", "Level_Id", c => c.Int());
            DropForeignKey("dbo.Skills", "JobOffer_Id", "dbo.JobOffers");
            DropForeignKey("dbo.JobOffers", "Company_Id", "dbo.Companies");
            DropIndex("dbo.Skills", new[] { "JobOffer_Id" });
            DropIndex("dbo.JobOffers", new[] { "Company_Id" });
            AlterColumn("dbo.Skills", "Level", c => c.Short(nullable: false));
            DropColumn("dbo.Skills", "JobOffer_Id");
            DropColumn("dbo.Languages", "Level");
            DropTable("dbo.JobOffers");
            DropTable("dbo.Companies");
            CreateIndex("dbo.Languages", "Level_Id");
            AddForeignKey("dbo.Languages", "Level_Id", "dbo.LanguageLevels", "Id");
        }
    }
}
