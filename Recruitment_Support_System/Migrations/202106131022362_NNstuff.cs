namespace Recruitment_Support_System.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NNstuff : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ExtendedUserJobOffers", "ExtendedUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ExtendedUserJobOffers", "JobOffer_Id", "dbo.JobOffers");
            DropIndex("dbo.ExtendedUserJobOffers", new[] { "ExtendedUser_Id" });
            DropIndex("dbo.ExtendedUserJobOffers", new[] { "JobOffer_Id" });
            CreateTable(
                "dbo.Applications",
                c => new
                    {
                        ExtendedUser_Id = c.String(nullable: false, maxLength: 128),
                        JobOffer_Id = c.Int(nullable: false),
                        GotInterview = c.Boolean(nullable: false),
                        GotJobOffer = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.ExtendedUser_Id, t.JobOffer_Id })
                .ForeignKey("dbo.AspNetUsers", t => t.ExtendedUser_Id, cascadeDelete: true)
                .ForeignKey("dbo.JobOffers", t => t.JobOffer_Id, cascadeDelete: true)
                .Index(t => t.ExtendedUser_Id)
                .Index(t => t.JobOffer_Id);
            
            DropTable("dbo.ExtendedUserJobOffers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ExtendedUserJobOffers",
                c => new
                    {
                        ExtendedUser_Id = c.String(nullable: false, maxLength: 128),
                        JobOffer_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ExtendedUser_Id, t.JobOffer_Id });
            
            DropForeignKey("dbo.Applications", "JobOffer_Id", "dbo.JobOffers");
            DropForeignKey("dbo.Applications", "ExtendedUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Applications", new[] { "JobOffer_Id" });
            DropIndex("dbo.Applications", new[] { "ExtendedUser_Id" });
            DropTable("dbo.Applications");
            CreateIndex("dbo.ExtendedUserJobOffers", "JobOffer_Id");
            CreateIndex("dbo.ExtendedUserJobOffers", "ExtendedUser_Id");
            AddForeignKey("dbo.ExtendedUserJobOffers", "JobOffer_Id", "dbo.JobOffers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ExtendedUserJobOffers", "ExtendedUser_Id", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
    }
}
