namespace Recruitment_Support_System.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Expectations : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Expectations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalaryFrom = c.Int(nullable: false),
                        SalaryTo = c.Int(nullable: false),
                        RWork = c.Boolean(nullable: false),
                        RRecruitment = c.Boolean(nullable: false),
                        ExtendedUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ExtendedUser_Id)
                .Index(t => t.ExtendedUser_Id);
            
            DropColumn("dbo.AspNetUsers", "Phone");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Phone", c => c.String());
            DropForeignKey("dbo.Expectations", "ExtendedUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Expectations", new[] { "ExtendedUser_Id" });
            DropTable("dbo.Expectations");
        }
    }
}
