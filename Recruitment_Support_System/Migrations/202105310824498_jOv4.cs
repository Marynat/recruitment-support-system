namespace Recruitment_Support_System.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class jOv4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.JobOffers", "ZarobkiMin", c => c.Long(nullable: false));
            AddColumn("dbo.JobOffers", "ZarobkiMax", c => c.Long(nullable: false));
            AddColumn("dbo.JobOffers", "OkresRozliczeniowy", c => c.Int(nullable: false));
            AddColumn("dbo.JobOffers", "Currency", c => c.String());
            AddColumn("dbo.JobOffers", "Remote", c => c.Boolean(nullable: false));
            AddColumn("dbo.JobOffers", "RemoteInterview", c => c.Boolean(nullable: false));
            AddColumn("dbo.JobOffers", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.JobOffers", "ExpirationDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.JobOffers", "ExpirationDate");
            DropColumn("dbo.JobOffers", "IsDeleted");
            DropColumn("dbo.JobOffers", "RemoteInterview");
            DropColumn("dbo.JobOffers", "Remote");
            DropColumn("dbo.JobOffers", "Currency");
            DropColumn("dbo.JobOffers", "OkresRozliczeniowy");
            DropColumn("dbo.JobOffers", "ZarobkiMax");
            DropColumn("dbo.JobOffers", "ZarobkiMin");
        }
    }
}
