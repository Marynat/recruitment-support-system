namespace Recruitment_Support_System.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class jOv3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.JobOffers", "Title", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.JobOffers", "Title");
        }
    }
}
