namespace Recruitment_Support_System.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CurriculumVitae : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Org = c.String(),
                        Passed = c.DateTime(nullable: false),
                        ValidTo = c.DateTime(nullable: false),
                        CV_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CVs", t => t.CV_Id)
                .Index(t => t.CV_Id);
            
            CreateTable(
                "dbo.CVs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Educations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Title = c.String(),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        CV_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CVs", t => t.CV_Id)
                .Index(t => t.CV_Id);
            
            CreateTable(
                "dbo.Interests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CV_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CVs", t => t.CV_Id)
                .Index(t => t.CV_Id);
            
            CreateTable(
                "dbo.Jobs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        Comment = c.String(),
                        Company = c.String(),
                        CV_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CVs", t => t.CV_Id)
                .Index(t => t.CV_Id);
            
            CreateTable(
                "dbo.Languages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CV_Id = c.Int(),
                        LanguageDictionary_Id = c.Int(),
                        Level_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CVs", t => t.CV_Id)
                .ForeignKey("dbo.LanguageDictionaries", t => t.LanguageDictionary_Id)
                .ForeignKey("dbo.LanguageLevels", t => t.Level_Id)
                .Index(t => t.CV_Id)
                .Index(t => t.LanguageDictionary_Id)
                .Index(t => t.Level_Id);
            
            CreateTable(
                "dbo.LanguageDictionaries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CountryCode = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LanguageLevels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Level = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Skills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Level = c.Short(nullable: false),
                        CV_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CVs", t => t.CV_Id)
                .Index(t => t.CV_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CVs", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Skills", "CV_Id", "dbo.CVs");
            DropForeignKey("dbo.Languages", "Level_Id", "dbo.LanguageLevels");
            DropForeignKey("dbo.Languages", "LanguageDictionary_Id", "dbo.LanguageDictionaries");
            DropForeignKey("dbo.Languages", "CV_Id", "dbo.CVs");
            DropForeignKey("dbo.Jobs", "CV_Id", "dbo.CVs");
            DropForeignKey("dbo.Interests", "CV_Id", "dbo.CVs");
            DropForeignKey("dbo.Educations", "CV_Id", "dbo.CVs");
            DropForeignKey("dbo.Courses", "CV_Id", "dbo.CVs");
            DropIndex("dbo.Skills", new[] { "CV_Id" });
            DropIndex("dbo.Languages", new[] { "Level_Id" });
            DropIndex("dbo.Languages", new[] { "LanguageDictionary_Id" });
            DropIndex("dbo.Languages", new[] { "CV_Id" });
            DropIndex("dbo.Jobs", new[] { "CV_Id" });
            DropIndex("dbo.Interests", new[] { "CV_Id" });
            DropIndex("dbo.Educations", new[] { "CV_Id" });
            DropIndex("dbo.CVs", new[] { "User_Id" });
            DropIndex("dbo.Courses", new[] { "CV_Id" });
            DropTable("dbo.Skills");
            DropTable("dbo.LanguageLevels");
            DropTable("dbo.LanguageDictionaries");
            DropTable("dbo.Languages");
            DropTable("dbo.Jobs");
            DropTable("dbo.Interests");
            DropTable("dbo.Educations");
            DropTable("dbo.CVs");
            DropTable("dbo.Courses");
        }
    }
}
