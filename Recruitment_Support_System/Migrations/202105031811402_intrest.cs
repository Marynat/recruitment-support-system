namespace Recruitment_Support_System.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class intrest : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Interests", newName: "Intrests");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Intrests", newName: "Interests");
        }
    }
}
