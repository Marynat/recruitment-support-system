namespace Recruitment_Support_System.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class jOv2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Skills", "JobOffer_Id", "dbo.JobOffers");
            DropIndex("dbo.Skills", new[] { "JobOffer_Id" });
            AddColumn("dbo.JobOffers", "Skill", c => c.String());
            DropColumn("dbo.Skills", "JobOffer_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Skills", "JobOffer_Id", c => c.Int());
            DropColumn("dbo.JobOffers", "Skill");
            CreateIndex("dbo.Skills", "JobOffer_Id");
            AddForeignKey("dbo.Skills", "JobOffer_Id", "dbo.JobOffers", "Id");
        }
    }
}
