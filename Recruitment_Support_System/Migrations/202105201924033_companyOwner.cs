namespace Recruitment_Support_System.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class companyOwner : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "Owner_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Companies", "Owner_Id");
            AddForeignKey("dbo.Companies", "Owner_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Companies", "Owner_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Companies", new[] { "Owner_Id" });
            DropColumn("dbo.Companies", "Owner_Id");
        }
    }
}
