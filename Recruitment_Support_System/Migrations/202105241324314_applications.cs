namespace Recruitment_Support_System.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class applications : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExtendedUserJobOffers",
                c => new
                    {
                        ExtendedUser_Id = c.String(nullable: false, maxLength: 128),
                        JobOffer_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ExtendedUser_Id, t.JobOffer_Id })
                .ForeignKey("dbo.AspNetUsers", t => t.ExtendedUser_Id, cascadeDelete: true)
                .ForeignKey("dbo.JobOffers", t => t.JobOffer_Id, cascadeDelete: true)
                .Index(t => t.ExtendedUser_Id)
                .Index(t => t.JobOffer_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ExtendedUserJobOffers", "JobOffer_Id", "dbo.JobOffers");
            DropForeignKey("dbo.ExtendedUserJobOffers", "ExtendedUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.ExtendedUserJobOffers", new[] { "JobOffer_Id" });
            DropIndex("dbo.ExtendedUserJobOffers", new[] { "ExtendedUser_Id" });
            DropTable("dbo.ExtendedUserJobOffers");
        }
    }
}
