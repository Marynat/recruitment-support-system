﻿using Microsoft.AspNet.Identity.EntityFramework;
using Recruitment_Support_System.Models;
using Recruitment_Support_System.Models.CurriculumVitae;
using Recruitment_Support_System.Models.Dictionaries;
using Recruitment_Support_System.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RssMachineLearning.Helpers;
using RssMachineLearning;
using System.Diagnostics;
using Recruitment_Support_System.Helpers;

namespace Recruitment_Support_System.App_Start
{
    public static class SetUpDatabase
    {
        public static double[] Weights;
        public static SingleHiddenLayerNeuralNetwork nn;

        private static int numInput = 4;
        private static int numHidden = 5;
        private static int numOutput = 3;
        private static int seed = 1;
        public static void SetUpEverything()
        {

            using (var db = new ApplicationDbContext())
            {
                var langdict1 = new LanguageDictionary() { Name = "Polski", CountryCode = "PL" };
                var langdict2 = new LanguageDictionary() { Name = "Angielski", CountryCode = "EN" };
                var langdict3 = new LanguageDictionary() { Name = "Hiszpański", CountryCode = "ES" };
                var langdict4 = new LanguageDictionary() { Name = "Niemiecki", CountryCode = "DE" };
                var langdict5 = new LanguageDictionary() { Name = "Francuski", CountryCode = "FR" };
                var langdict6 = new LanguageDictionary() { Name = "Szwedzki", CountryCode = "SW" };
                var langdict7 = new LanguageDictionary() { Name = "Rosyjski", CountryCode = "RU" };

                if (!db.LanguageDictionary.Any())
                {
                    db.LanguageDictionary.Add(langdict1);
                    db.LanguageDictionary.Add(langdict2);
                    db.LanguageDictionary.Add(langdict3);
                    db.LanguageDictionary.Add(langdict4);
                    db.LanguageDictionary.Add(langdict5);
                    db.LanguageDictionary.Add(langdict6);
                    db.LanguageDictionary.Add(langdict7);
                }

                if (!db.CVs.Any())
                {
                    CV cV1 = new CV() { User = db.Users.Where(u => u.Id == "da75ba13-8eee-451b-ae8b-78779363f141").First() };
                    //cV1.Jobs = new List<Job>();
                    var job1 = new Job() { Id = 1, CV = cV1, Name = "Super Praca", Company = "Super firma", Start = DateTime.Today.AddMonths(-20), End = DateTime.Today.AddMonths(-8), Comment = "extra" };
                    var job2 = new Job() { Id = 2, CV = cV1, Name = "Super super", Company = "Super cuper", Start = DateTime.Today.AddMonths(-8), End = DateTime.Today.AddMonths(-3), Comment = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." };
                    var job3 = new Job() { Id = 3, CV = cV1, Name = "Super super super", Company = "Super asdjkfsafs", Start = DateTime.Today.AddMonths(-3), End = DateTime.Today, Comment = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." };
                    //cV1.Educations = new List<Education>();
                    var edu1 = new Education() { Id = 0, CV = cV1, Name = "super szkoła", Title = "MGR", Start = DateTime.Today.AddMonths(-20), End = DateTime.Today.AddMonths(-8) };
                    //cV1.Interests = new List<Intrest>();
                    var int1 = new Intrest() { Id = 0, CV = cV1, Name = "Internet" };
                    //cV1.Languages = new List<Language>();
                    var lang1 = new Language() { Id = 0, CV = cV1, LanguageDictionary = langdict1, Level = LanguageLevel.Ojczysty };
                    var lang2 = new Language() { Id = 1, CV = cV1, LanguageDictionary = langdict2, Level = LanguageLevel.Zaawansowany };
                    //cV1.Skills = new List<Skill>();
                    var skill1 = new Skill() { Id = 0, CV = cV1, Name = ".Net", Level = SkillLevel.Podstawowy };
                    //cV1.Courses = new List<Course>();
                    var course1 = new Course() { Id = 0, CV = cV1, Name = "jakis kurs", Org = "super", Passed = DateTime.Today.AddMonths(-20), ValidTo = DateTime.Today.AddMonths(20) };
                    var course2 = new Course() { Id = 1, CV = cV1, Name = "Super  kurs", Org = "Mega super", Passed = DateTime.Today.AddMonths(-20), ValidTo = DateTime.Today };
                    db.CVs.Add(cV1);
                    db.Jobs.Add(job1);
                    db.Jobs.Add(job2);
                    db.Jobs.Add(job3);
                    db.Educations.Add(edu1);
                    db.Interests.Add(int1);
                    db.Languages.Add(lang1);
                    db.Languages.Add(lang2);
                    db.Skills.Add(skill1);
                    db.Courses.Add(course1);
                    db.Courses.Add(course2);
                }

                if (!db.Roles.Any())
                {
                    db.Roles.Add(new IdentityRole() { Name = "Admin" });
                    db.Roles.Add(new IdentityRole() { Name = "User" });
                    db.Roles.Add(new IdentityRole() { Name = "Company" });
                }

                if (!db.JobOffers.Any())
                {
                    db.JobOffers.Add(new JobOffer() { Company = new Company() { } });
                }
                db.SaveChanges();
                nn = new SingleHiddenLayerNeuralNetwork(numInput, numHidden, numOutput);
            }

        }

        public static void LearnFromCurrentDataSet()
        {
            Debug.WriteLine("\nBegin neural network back-propagation demo");



            Debug.WriteLine("\nGenerating data items with " + numInput + " features");
            double[][] allData = GetAndPrepareDataForML();
            Debug.WriteLine("Done");

            DataHelper.ShowMatrix(allData, 4, 2, true);

            Debug.WriteLine("\nCreating train (80%) and test (20%) matrices");
            double[][] trainData;
            double[][] testData;
            DataHelper.SplitTrainTest(allData, 0.80, seed, out trainData, out testData);
            Debug.WriteLine("Done\n");

            Debug.WriteLine("Training data:");
            DataHelper.ShowMatrix(trainData, 4, 2, true);
            Debug.WriteLine("Test data:");
            DataHelper.ShowMatrix(testData, 4, 2, true);

            Debug.WriteLine("Creating a " + numInput + "-" + numHidden +
              "-" + numOutput + " neural network");

            Debug.WriteLine("\nFirst NN weights and Biases:\n");
            DataHelper.ShowVector(nn.GetWeights(), 6, 10, true);

            int maxEpochs = 2000;
            double learnRate = 0.02;
            double momentum = 0.01;
            Debug.WriteLine("\nSetting maxEpochs = " + maxEpochs);
            Debug.WriteLine("Setting learnRate = " + learnRate.ToString("F2"));
            Debug.WriteLine("Setting momentum  = " + momentum.ToString("F2"));

            Debug.WriteLine("\nStarting training");
            double[] weights = nn.Train(trainData, maxEpochs, learnRate, momentum);
            Weights = weights;
            Debug.WriteLine("Done");
            Debug.WriteLine("\nFinal neural network model weights and biases:\n");
            DataHelper.ShowVector(weights, 2, 10, true);

            //double[] y = nn.ComputeOutputs(new double[] { 1.0, 2.0, 3.0, 4.0 });
            //ShowVector(y, 3, 3, true);

            double trainAcc = nn.Accuracy(trainData);
            Debug.WriteLine("\nFinal accuracy on training data = " +
              trainAcc.ToString("F4"));

            double testAcc = nn.Accuracy(testData);
            Debug.WriteLine("Final accuracy on test data     = " +
              testAcc.ToString("F4"));

            Debug.WriteLine("\nEnd back-propagation demo\n");
        }

        private static double[][] GetAndPrepareDataForML()
        {

            using (var db = new ApplicationDbContext())
            {
                var apps = db.ExtendedUserJobOffers.Include("ExtendedUser")
                                                   .Include("JobOffer")
                                                   .ToList();
                double[][] result = new double[apps.Count][];
                int i = 0;
                foreach (var a in apps)
                {
                    result[i++] = AttributeHelper.getSingleRowWithOut(a);
                }
                return result;
            }
        }

    }
}