﻿using Microsoft.AspNet.Identity;
using Recruitment_Support_System.Helpers;
using Recruitment_Support_System.Models;
using Recruitment_Support_System.Models.CurriculumVitae;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Recruitment_Support_System.Controllers
{
    public class CompanyController : Controller
    {
        // GET: Company
        public ActionResult Index()
        {
            return View();
        }
        [Authorize(Roles = "Company")]
        public ActionResult Offer(JobOffer jobOffer)
        {
            using (var db = new ApplicationDbContext())
            {
                jobOffer.IsDeleted = false;
                if (db.JobOffers.Any(j => j.Id == jobOffer.Id))
                {
                    var jO = db.JobOffers.FirstOrDefault(j => j.Id == jobOffer.Id);
                    UpdateHelper.UpdateJobOffer(jO, jobOffer);
                }
                else
                {
                    var user = User.Identity.GetUserId();
                    jobOffer.Company = db.Companies.FirstOrDefault(c => c.Owner.Id == user);
                    db.JobOffers.Add(jobOffer);
                }
                db.SaveChanges();
                return RedirectToAction("Index", "Jobs");
            }
        }

        [HttpGet]
        //[Authorize]
        public async Task<ActionResult> CVPreview(string id)
        {
            using (var db = new ApplicationDbContext())
            {
                CV cV = db.CVs.Include("User").Include("Courses").Include("Jobs").Include("Intrests").Include("Educations").Include("Languages.LanguageDictionary").Include("Skills").FirstOrDefault(cv => cv.User.Id == id);
                if (cV != null)
                {
                    cV.LanguagesDictionary = db.LanguageDictionary.ToList();
                }

                return View(cV);
            }
        }
        [Authorize]
        public async Task<ActionResult> CV(string id)
        {
            var report = new ActionAsPdf("CVPreview", new { id = id }) { PageOrientation = Rotativa.Options.Orientation.Landscape };
            return report;
        }

    }
}