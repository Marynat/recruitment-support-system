﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Recruitment_Support_System.Helpers;
using Recruitment_Support_System.Models;
using Recruitment_Support_System.Models.Builders;
using Recruitment_Support_System.Models.Builders.CvBuilders;
using Recruitment_Support_System.Models.Builders.Direcors;
using Recruitment_Support_System.Models.CurriculumVitae;
using Recruitment_Support_System.Models.Dictionaries;
using Recruitment_Support_System.Models.Enums;
using Recruitment_Support_System.Models.ViewModels;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Recruitment_Support_System.Controllers
{
    [Authorize]
    public class CvController : Controller
    {
        private ApplicationUserManager _uM;
        private ApplicationDbContext dbContext;

        public Func<string> GetUserId;

        public ApplicationDbContext db { get => dbContext ?? new ApplicationDbContext(); set => dbContext = value; }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _uM ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _uM = value;
            }
        }

        public CvController()
        {

            GetUserId = () => User.Identity.GetUserId();
        }
        public CvController(ApplicationUserManager userManager)
        {
            _uM = userManager;
            GetUserId = () => User.Identity.GetUserId();
        }

        // GET: Cv
        [HttpGet]
        [Authorize(Roles = "User")]
        public async Task<ActionResult> Index()
        {
            CV cV = (CV)Session["cV"];
            if (cV == null)
            {
                var user = await UserManager.FindByIdAsync(GetUserId());
                var userId = GetUserId();
                //var user = db.Users.First(u => u.Id == userId);
                cV = db.CVs.Include("User").Include("Courses").Include("Jobs").Include("Intrests").Include("Educations").Include("Languages.LanguageDictionary").Include("Skills").FirstOrDefault(cv => cv.User.Id == user.Id);
                if (cV != null)
                {
                    cV.LanguagesDictionary = db.LanguageDictionary.ToList();
                }
                else
                {
                    cV = new CV();
                    cV.LanguagesDictionary.AddRange(db.LanguageDictionary.ToList());
                }
                cV.LanguagesDictionary.Add(new LanguageDictionary() { Name = "Inny", CountryCode = "CC" });

                Session["cV"] = cV;
            }
            else
            {
                cV = (CV)Session["cV"];
            }

            return View(cV);

        }

        [Authorize(Roles = "User")]
        public ActionResult Job(Job job)
        {
            CV cV = (CV)Session["cV"];
            Job tmpJob = null;
            try
            {
                tmpJob = cV.Jobs.Where(i => i.Id == job.Id).First();
            }
            catch { }
            if (tmpJob == null)
            {
                cV.Jobs.Add(new Job() { CV = cV, Id = cV.Jobs.Any() ? cV.Jobs.Last().Id + 1 : 1, Name = job.Name, Company = job.Company, Comment = job.Comment, End = job.End, Start = job.Start });
            }
            else
            {
                UpdateHelper.UpdateJob(tmpJob, job);
            }
            Session["cV"] = cV;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "User")]
        public ActionResult DeleteJob(int id)
        {
            CV cV = (CV)Session["cV"];
            cV.Jobs.Remove(cV.Jobs.Where(j => j.Id == id).First());
            Session["cV"] = cV;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "User")]
        public ActionResult Education(Education education)
        {
            CV cV = (CV)Session["cV"];
            Education tmpEdu = null;
            try
            {
                tmpEdu = cV.Educations.Where(i => i.Id == education.Id).First();
            }
            catch { }
            if (tmpEdu == null)
            {
                cV.Educations.Add(new Education() { CV = cV, Id = cV.Educations.Any() ? cV.Educations.Last().Id + 1 : 1, Name = education.Name, Title = education.Title, End = education.End, Start = education.Start });
            }
            else
            {
                UpdateHelper.UpdateEducation(tmpEdu, education);
            }
            Session["cV"] = cV;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "User")]
        public ActionResult DeleteEducation(int id)
        {
            CV cV = (CV)Session["cV"];
            cV.Educations.Remove(cV.Educations.Where(j => j.Id == id).First());
            Session["cV"] = cV;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "User")]
        public ActionResult Course(Course course)
        {
            CV cV = (CV)Session["cV"];
            Course tmpCour = null;
            try
            {
                tmpCour = cV.Courses.Where(i => i.Id == course.Id).First();
            }
            catch { }
            if (tmpCour == null)
            {
                cV.Courses.Add(new Course() { CV = cV, Id = cV.Educations.Any() ? cV.Educations.Last().Id + 1 : 1, Name = course.Name, Org = course.Org, Passed = course.Passed, ValidTo = course.ValidTo });
            }
            else
            {
                UpdateHelper.UpdateCourse(tmpCour, course);
            }
            Session["cV"] = cV;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "User")]
        public ActionResult DeleteCourse(int id)
        {
            CV cV = (CV)Session["cV"];
            cV.Courses.Remove(cV.Courses.Where(j => j.Id == id).First());
            Session["cV"] = cV;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "User")]
        public ActionResult Skill(Skill skill)
        {
            CV cV = (CV)Session["cV"];
            Skill tmpSkill = null;
            try
            {
                tmpSkill = cV.Skills.Where(i => i.Id == skill.Id).First();
            }
            catch { }
            if (tmpSkill == null)
            {
                cV.Skills.Add(new Skill() { CV = cV, Id = cV.Educations.Any() ? cV.Educations.Last().Id + 1 : 1, Name = skill.Name, Level = skill.Level });
            }
            else
            {
                UpdateHelper.UpdateSkill(tmpSkill, skill);
            }
            Session["cV"] = cV;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "User")]
        public ActionResult DeleteSkill(int id)
        {
            CV cV = (CV)Session["cV"];
            cV.Skills.Remove(cV.Skills.Where(j => j.Id == id).First());
            Session["cV"] = cV;
            return RedirectToAction("Index");
        }
        [HttpPost]
        public void AddNewLanguage(int id, string name)
        {
            CV cV = (CV)Session["cV"];
            cV.LanguagesDictionary.Add(new LanguageDictionary() { Id = id, Name = name, CountryCode = name.Substring(0, 3) });
            Session["cV"] = cV;
        }

        [Authorize(Roles = "User")]
        public ActionResult Language(LanguageViewModel langu)
        {

            CV cV = (CV)Session["cV"];

            LanguageDictionary newLanguage = null;
            try
            {
                newLanguage = cV.LanguagesDictionary.Where(i => i.Id == langu.Language.Id).First();
            }
            catch { }
            //if(newLanguage == null)
            //{
            //    newLanguage = new LanguageDictionary() { Id = langu.Language.Id, Name = langu.Language.Name, CountryCode = langu.Language.Name.Substring(0,3) };
            //    cV.LanguagesDictionary.Add(newLanguage);
            //}

            Language tmpLang = null;
            try
            {
                tmpLang = cV.Languages.Where(i => i.Id == langu.MyLanguageId).First();
            }
            catch { }
            if (tmpLang == null)
            {
                cV.Languages.Add(new Language() { CV = cV, LanguageDictionary = newLanguage, Id = cV.Languages.Any() ? cV.Languages.Last().Id + 1 : 1, Level = langu.Level });
            }
            else
            {
                tmpLang.Level = langu.Level;
            }
            Session["cV"] = cV;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "User")]
        public ActionResult DeleteLanguage(int id)
        {
            CV cV = (CV)Session["cV"];
            cV.Languages.Remove(cV.Languages.Where(j => j.Id == id).First());
            Session["cV"] = cV;
            return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        public ActionResult Intrest(Intrest inter)
        {

            CV cV = (CV)Session["cV"];
            Intrest tmpInt = null;
            try
            {
                tmpInt = cV.Intrests.Where(i => i.Id == inter.Id).First();
            }
            catch { }
            if (tmpInt == null)
            {
                cV.Intrests.Add(new Intrest() { CV = cV, Name = inter.Name, Id = cV.Intrests.Any() ? cV.Intrests.Last().Id + 1 : 1 });
            }
            else
            {
                tmpInt.Name = inter.Name;
            }
            Session["cV"] = cV;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "User")]
        public ActionResult DeleteIntrest(int id)
        {
            CV cV = (CV)Session["cV"];
            cV.Intrests.Remove(cV.Intrests.Where(j => j.Id == id).First());
            Session["cV"] = cV;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "User")]
        public ActionResult Save()
        {
            CV cV = (CV)Session["cV"];
            var cVBuilder = new DatabseCVBuilder(cV, User.Identity.GetUserId());
            CvDirector director = new CvDirector(cVBuilder);
            director.BuildFullCV(cV);
            return RedirectToAction("Index");
        }
    }
}