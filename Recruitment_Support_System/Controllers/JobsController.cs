﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Recruitment_Support_System.App_Start;
using Recruitment_Support_System.Helpers;
using Recruitment_Support_System.Models;
using Recruitment_Support_System.Models.ViewModels;
using RssMachineLearning;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Recruitment_Support_System.Controllers
{
    [Authorize]
    public class JobsController : Controller
    {

        private ApplicationUserManager _userManager;
        private SingleHiddenLayerNeuralNetwork nn;

        public Func<string> GetUserId;
        public JobsController()
        {
            GetUserId = () => User.Identity.GetUserId();
        }

        public JobsController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
            GetUserId = () => User.Identity.GetUserId();
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Jobs
        [Authorize(Roles = "Company")]
        public async Task<ActionResult> Index()
        {
            var model = new List<JobOffer>();
            using (var db = new ApplicationDbContext())
            {
                var uid = GetUserId();
                var user = await UserManager.FindByIdAsync(uid);
                model = db.JobOffers.Where(j => j.Company.Owner.Id == user.Id).ToList();
                return View(model);
            }
        }

        [AllowAnonymous]
        [HandleError(ExceptionType = typeof(ArgumentException), View = "NotFound")]
        public ActionResult JobOffer(int? id, string returnUrl)
        {
            if (id == null)
            {
                throw new HttpException(404, "Not found");
            }
            if (string.IsNullOrEmpty(returnUrl))
            {
                returnUrl = "Home+Index";
            }
            using (var db = new ApplicationDbContext())
            {
                var userId = GetUserId();
                var returnUrls = returnUrl.Split('+');
                ViewBag.ReturnControllerUrl = returnUrls[0];
                ViewBag.ReturnUrl = returnUrls[1];
                var offer = db.JobOffers.Include("Company").Include("Applications").FirstOrDefault(jo => jo.Id == id);
                if (offer == null)
                {
                    throw new HttpException(404, "Not found");
                }
                if (offer.IsDeleted)
                {
                    ViewBag.Message = "Dana oferta już wygasła lub została usunięta. Nie można jej podejrzeć.";
                }
                JobViewModel model = new JobViewModel(offer);
                if (offer.Applications.Any(a => a.ExtendedUser_Id == userId))
                {
                    model.Applied = true;
                }
                else model.Applied = false;
                return View(model);

            }
        }

        public async Task<ActionResult> Applications(int id)
        {
            Dictionary<Application, double> keys = new Dictionary<Application, double>();
            nn = SingleHiddenLayerNeuralNetwork.GetInststance();
            using (var db = new ApplicationDbContext())
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                var model = db.Users.Include("Applications").Where(u => u.Applications.Any(a => a.JobOffer.Company.Owner.Id == user.Id && a.JobOffer_Id == id)).Distinct().ToList();
                List<ApplicationsViewModel> viewModel = new List<ApplicationsViewModel>();
                int i = 0;
                double[][] outputs = new double[model.Count][];
                foreach (var u in model)
                {
                    var appl = u.Applications.FirstOrDefault(a => a.JobOffer_Id == id);
                    outputs[i++] = nn.GenerateOutputs(AttributeHelper.getSingleRow(appl));
                    keys.Add(appl, AttributeHelper.addOutputValues(outputs[i - 1]));
                }


                var keyss = keys.OrderByDescending(k => k.Value);
                int j = 0;
                foreach (var k in keyss)
                {
                    var mod = db.Users.FirstOrDefault(e => e.Id == k.Key.ExtendedUser_Id);
                    ApplicationsViewModel exModel = new ApplicationsViewModel(mod, k.Key ?? new Application(), id);
                    viewModel.Add(exModel);
                }

                return View(viewModel);
            }
        }
        [Authorize]
        public async Task<ActionResult> Apply(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var userId = User.Identity.GetUserId();
                var user = db.Users.FirstOrDefault(u => u.Id == userId);
                if (!db.CVs.Any(c => c.User.Id == userId))
                {
                    throw new HttpException(404, "Nie znaleziono CV - najpierw stwórz swoje CV");
                }
                var jO = db.JobOffers.Include("Company").Include("Applications").FirstOrDefault(j => j.Id == id);
                if (jO == null)
                {
                    throw new HttpException(403, "Bad request");
                }
                jO.Applications.Add(new Application() { ExtendedUser_Id = userId, JobOffer_Id = jO.Id });
                db.SaveChanges();
                return View(new ApplyViewModel() { CompanyName = jO.Company.Name, JobTitle = jO.Title });
            }
        }
        [Authorize(Roles = "Company")]
        public async Task<ActionResult> DeleteOffer(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var jO = db.JobOffers.FirstOrDefault(j => j.Id == id);
                jO.IsDeleted = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        public JsonResult UpdateGotInterview(bool check, string uid, int joId)
        {
            var currUserId = User.Identity.GetUserId();
            try
            {
                using (var db = new ApplicationDbContext())
                {

                    var user = db.Users.Include("Applications").FirstOrDefault(u => u.Id == uid);
                    var company = db.Companies.FirstOrDefault(c => c.Owner.Id == currUserId);
                    var app = user.Applications.FirstOrDefault(a => a.JobOffer_Id == joId);
                    app.GotInterview = !app.GotInterview;
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {
                return Json("Fail");
            }
            return Json("Success");
        }
        public JsonResult UpdateGotJob(bool check, string uid, int joId)
        {
            var currUserId = User.Identity.GetUserId();
            try
            {
                using (var db = new ApplicationDbContext())
                {

                    var user = db.Users.Include("Applications").FirstOrDefault(u => u.Id == uid);
                    var company = db.Companies.FirstOrDefault(c => c.Owner.Id == currUserId);
                    var app = user.Applications.FirstOrDefault(a => a.JobOffer_Id == joId);
                    app.GotJobOffer = !app.GotJobOffer;
                    db.SaveChanges();
                }
            }
            catch (Exception) { return Json("Fail"); }

            return Json("Success");
        }
    }

}