﻿using Recruitment_Support_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Recruitment_Support_System.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Opis projektu.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Informacje o autorze.";
            return View();
        }

        public ActionResult JobOffers()
        {
            using (var db = new ApplicationDbContext())
                return View(db.JobOffers.Include("Company").Where(j=>j.IsDeleted != true).ToList());
        }
    }
}