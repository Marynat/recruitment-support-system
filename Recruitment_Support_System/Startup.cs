﻿using Microsoft.Owin;
using Owin;
using Recruitment_Support_System.App_Start;
using Recruitment_Support_System.ScheduledTasks;

[assembly: OwinStartupAttribute(typeof(Recruitment_Support_System.Startup))]
namespace Recruitment_Support_System
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            SetUpDatabase.SetUpEverything();
            SetUpDatabase.LearnFromCurrentDataSet();
            JobScheduler.Start();
        }
    }
}
