﻿using Quartz;
using Recruitment_Support_System.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Recruitment_Support_System.ScheduledTasks
{
    public class LearningJob : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            SetUpDatabase.LearnFromCurrentDataSet();

            return Task.CompletedTask;
        }
    }
}