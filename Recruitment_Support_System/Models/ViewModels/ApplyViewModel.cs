﻿using Recruitment_Support_System.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Models.ViewModels
{
    public class ApplyViewModel
    {
        public string CompanyName { get; set; }
        public string JobTitle { get; set; }
    }

    [NotMapped]
    public class ApplicationsViewModel : ExtendedUser
    {
        private bool gotInterview = false;
        private bool gotJob = false;
        private int joId;
        private string jobLevel;

        [NotMapped]
        [Display(Name = "Zaproszony na rozmowę kwalifikacyjną")]
        public bool GotInteview { get => gotInterview; set => gotInterview = value; }
        [NotMapped]
        [Display(Name = "Oferta pracy")]
        public bool GotJob { get => gotJob; set => gotJob = value; }
        [NotMapped]
        public int JoId { get => joId; set => joId = value; }
        [NotMapped]
        public string JobTitle { get; set; }
        public string JobLevel { get => jobLevel; set => jobLevel = value; }

        public ApplicationsViewModel() { }

        public ApplicationsViewModel(ExtendedUser u, Application a, int id)
        {
            this.AccessFailedCount = u.AccessFailedCount;
            this.Age = u.Age;
            this.Applications = u.Applications;
            this.Birthdate = u.Birthdate;
            this.CVs = u.CVs;
            this.Email = u.Email;
            this.EmailConfirmed = u.EmailConfirmed;
            this.GotInteview = a.GotInterview;
            this.GotJob = a.GotJobOffer;
            this.Id = u.Id;
            this.Name = u.Name;
            this.PhoneNumber = u.PhoneNumber;
            this.Surname = u.Surname;
            this.UserName = u.UserName;
            JoId = id;

            using (var db = new ApplicationDbContext())
            {
                JobTitle = db.JobOffers.FirstOrDefault(j => j.Id == JoId).Title;
                JobLevel = db.JobOffers.FirstOrDefault().JobLevel.ToString();
            }
        }
    }
}