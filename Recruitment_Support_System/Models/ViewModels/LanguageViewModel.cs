﻿using Recruitment_Support_System.Models.Dictionaries;
using Recruitment_Support_System.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Recruitment_Support_System.Models.ViewModels
{
    public class LanguageViewModel
    {
        public int MyLanguageId { get; set; }
        public LanguageDictionary Language { get; set; }
        public IEnumerable<SelectListItem> Languages { get; set; }
        public LanguageLevel Level { get; set; }

    }
}