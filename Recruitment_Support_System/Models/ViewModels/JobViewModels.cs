﻿using Recruitment_Support_System.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Models.ViewModels
{
    [NotMapped]
    public class JobViewModel : JobOffer
    {
        [NotMapped]
        public bool Applied { get; set; }

        public JobViewModel(JobOffer offer)
        {
            base.About = offer.About;
            base.Applications = offer.Applications;
            base.Company = offer.Company;
            base.Currency = offer.Currency;
            base.Id = offer.Id;
            base.JobContract = offer.JobContract;
            base.JobLevel = offer.JobLevel;
            base.OkresRozliczeniowy = offer.OkresRozliczeniowy;
            base.Remote = offer.Remote;
            base.RemoteInterview = offer.RemoteInterview;
            base.Requirements = offer.Requirements;
            base.Responsibilities = offer.Responsibilities;
            base.Skill = offer.Skill;
            base.Skills = offer.Skills;
            base.TeamSize = offer.TeamSize;
            base.Title = offer.Title;
            base.ZarobkiMax = offer.ZarobkiMax;
            base.ZarobkiMin = offer.ZarobkiMin;
            base.IsDeleted = offer.IsDeleted;
            base.ExpirationDate = ExpirationDate;
            Applied = false;
        }
    }
}