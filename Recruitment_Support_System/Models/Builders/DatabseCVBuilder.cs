﻿using Recruitment_Support_System.Helpers;
using Recruitment_Support_System.Models.Builders.CvBuilders;
using Recruitment_Support_System.Models.CurriculumVitae;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Models.Builders
{
    public class DatabseCVBuilder : ICVBuilder
    {
        private int id;
        private CV _cv;
        private ApplicationDbContext dbContext;

        public ApplicationDbContext DbContext { get => dbContext ?? new ApplicationDbContext(); set => dbContext = value; }
        public CV Cv { get => _cv ?? new CV(); set => _cv = value; }

        //TODO: Rest of this
        public DatabseCVBuilder(CV cV, string uId)
        {

            DbContext = new ApplicationDbContext();
            if (DbContext.CVs.Any(c => c.User.Id == uId))
            {
                Cv = DbContext.CVs.Include("User").First(c => c.User.Id == uId);
            }
            else
            {
                Cv = new CV();
                Cv.User = DbContext.Users.First(u => u.Id == uId);
                DbContext.CVs.Add(Cv);
                try
                {
                    DbContext.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    // Retrieve the error messages as a list of strings.
                    var errorMessages = ex.EntityValidationErrors
                            .SelectMany(x => x.ValidationErrors)
                            .Select(x => x.ErrorMessage);

                    // Join the list to a single string.
                    var fullErrorMessage = string.Join("; ", errorMessages);

                    // Combine the original exception message with the new one.
                    var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                    // Throw a new DbEntityValidationException with the improved exception message.
                    throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                }
            }
            id = Cv.Id;
        }

        public void BuildCourses(IEnumerable<Course> courses)
        {
            if (DbContext.Courses.Any(c => c.CV.Id == id))
            {
                var currentCourses = DbContext.Courses.Where(c => c.CV.Id == id).ToList();
                var tmpCour = currentCourses.Except(courses, new ComparerHelper()).ToList();
                foreach (var cur in tmpCour)
                {
                    DbContext.Courses.Remove((Course)cur);
                }
            }

            foreach (var course in courses)
            {
                course.CV = Cv;
                if (!DbContext.Courses.Any(c => c.Id == course.Id))
                {
                    DbContext.Courses.Add(course);
                }
                else
                {
                    var c = DbContext.Courses.FirstOrDefault(x => x.Id == course.Id);
                    UpdateHelper.UpdateCourse(c, course);
                }
            }
            DbContext.SaveChanges();

        }

        public void BuildEducation(IEnumerable<Education> educations)
        {

            var dbEd = DbContext.Educations.Where(e => e.CV.Id == id).ToList();
            var tmpEd = dbEd.Except(educations, new ComparerHelper()).ToList();

            foreach (var ed in tmpEd)
            {
                DbContext.Educations.Remove((Education)ed);
            }

            foreach (var ed in educations)
            {
                ed.CV = Cv;
                if (!DbContext.Educations.Any(e => e.Id == ed.Id))
                    DbContext.Educations.Add(ed);
                else
                {
                    var e = DbContext.Educations.FirstOrDefault(x => x.Id == ed.Id);
                    UpdateHelper.UpdateEducation(e, ed);
                }

            }
            DbContext.SaveChanges();

        }

        public void BuildIntrests(IEnumerable<Intrest> intrests)
        {

            var dbInt = DbContext.Interests.Where(e => e.CV.Id == id).ToList();
            var tmpInt = dbInt.Except(intrests, new ComparerHelper()).ToList();

            foreach (var ed in tmpInt)
            {
                DbContext.Interests.Remove((Intrest)ed);
            }

            foreach (var i in intrests)
            {
                i.CV = Cv;
                if (!DbContext.Interests.Any(e => e.Id == i.Id))
                    DbContext.Interests.Add(i);
                else
                {
                    var e = DbContext.Interests.FirstOrDefault(x => x.Id == i.Id);
                    e.Name = i.Name;
                }

            }
            DbContext.SaveChanges();

        }

        public void BuildJobs(IEnumerable<Job> jobs)
        {

            var dbJobs = DbContext.Jobs.Where(e => e.CV.Id == id).ToList();
            var tmpJobs = dbJobs.Except(jobs, new ComparerHelper()).ToList();

            foreach (var jo in tmpJobs)
            {
                DbContext.Jobs.Remove((Job)jo);
            }

            foreach (var j in jobs)
            {
                j.CV = Cv;
                if (!DbContext.Jobs.Any(e => e.Id == j.Id))
                    DbContext.Jobs.Add(j);
                else
                {
                    var e = DbContext.Jobs.FirstOrDefault(x => x.Id == j.Id);
                    UpdateHelper.UpdateJob(e, j);
                }

            }
            try
            {
                DbContext.SaveChanges();
            }
            catch (Exception e)
            {

            }

        }

        public void BuildLanguages(IEnumerable<Language> languages)
        {

            var dbLangs = DbContext.Languages.Where(e => e.CV.Id == id).ToList();
            var tmpLangs = dbLangs.Except(languages, new ComparerHelper()).ToList();

            foreach (var lang in tmpLangs)
            {
                DbContext.Languages.Remove((Language)lang);
            }

            foreach (var l in languages)
            {
                l.CV = Cv;
                if (!DbContext.Languages.Any(e => e.Id == l.Id))
                    DbContext.Languages.Add(l);
                else
                {
                    var e = DbContext.Languages.FirstOrDefault(x => x.Id == l.Id);
                    e.Level = l.Level;
                }

            }
            DbContext.SaveChanges();

        }

        public void BuildSkills(IEnumerable<Skill> skills)
        {

            var dbSkills = DbContext.Skills.Where(e => e.CV.Id == id).ToList();
            var tmpSkill = dbSkills.Except(skills, new ComparerHelper()).ToList();

            foreach (var lang in tmpSkill)
            {
                DbContext.Skills.Remove((Skill)lang);
            }

            foreach (var s in skills)
            {
                s.CV = Cv;
                if (!DbContext.Skills.Any(e => e.Id == s.Id))
                    DbContext.Skills.Add(s);
                else
                {
                    var e = DbContext.Skills.FirstOrDefault(x => x.Id == s.Id);
                    UpdateHelper.UpdateSkill(e, s);
                }

            }
            DbContext.SaveChanges();

        }

        public void Reset()
        {
            Cv = new CV();
        }

        public void BuildCv()
        {


            //_cv.User = DbContext.Users.First(c => c.Id == UserId);
            //try
            //{
            //    DbContext.SaveChanges();
            //}
            //catch (DbEntityValidationException ex)
            //{
            //    // Retrieve the error messages as a list of strings.
            //    var errorMessages = ex.EntityValidationErrors
            //            .SelectMany(x => x.ValidationErrors)
            //            .Select(x => x.ErrorMessage);

            //    // Join the list to a single string.
            //    var fullErrorMessage = string.Join("; ", errorMessages);

            //    // Combine the original exception message with the new one.
            //    var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

            //    // Throw a new DbEntityValidationException with the improved exception message.
            //    throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            //}

        }
    }
}