﻿using Recruitment_Support_System.Models.Builders.CvBuilders;
using Recruitment_Support_System.Models.CurriculumVitae;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Models.Builders.Direcors
{
    public class CvDirector
    {
        private ICVBuilder cVBuilder;

        public CvDirector(ICVBuilder builder)
        {
            cVBuilder = builder;
        }

        public void BuildBasicCV(CV cV)
        {
            cVBuilder.BuildEducation(cV.Educations);
            cVBuilder.BuildJobs(cV.Jobs);
            cVBuilder.BuildLanguages(cV.Languages);
            cVBuilder.BuildSkills(cV.Skills);
        }

        public void BuildFullCV(CV cV)
        {
            cVBuilder.BuildJobs(cV.Jobs);
            cVBuilder.BuildEducation(cV.Educations);
            cVBuilder.BuildCourses(cV.Courses);
            cVBuilder.BuildIntrests(cV.Intrests);
            cVBuilder.BuildLanguages(cV.Languages);
            cVBuilder.BuildSkills(cV.Skills);
        }
    }
}