﻿using Recruitment_Support_System.Models.CurriculumVitae;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment_Support_System.Models.Builders.CvBuilders
{
    public interface ICVBuilder
    {
        void BuildCourses(IEnumerable<Course> courses);
        void BuildEducation(IEnumerable<Education> educations);
        void BuildIntrests(IEnumerable<Intrest> intrests);
        void BuildJobs(IEnumerable<Job> jobs);
        void BuildLanguages(IEnumerable<Language> languages);
        void BuildSkills(IEnumerable<Skill> skills);
    }
}
