﻿using Recruitment_Support_System.Models.CurriculumVitae.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Models.CurriculumVitae
{
    public class Intrest : ICvId
    {
        private int id;
        private CV cV;
        private string name;

        [Key]
        public int Id { get => id; set => id = value; }
        public CV CV { get => cV; set => cV = value; }
        [Display(Name = "Nazwa")]
        public string Name { get => name; set => name = value; }
    }
}