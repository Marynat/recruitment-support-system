﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment_Support_System.Models.CurriculumVitae.Interfaces
{
    public interface ICvId
    {
        int Id { get; set; }
    }
}
