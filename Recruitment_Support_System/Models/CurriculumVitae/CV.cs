﻿using Recruitment_Support_System.Models.Dictionaries;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Models.CurriculumVitae
{
    public class CV
    {
        private int id;
        private string user_Id;
        private ExtendedUser user;
        private List<Job> jobs;
        private List<Education> educations;
        private List<Language> languages;
        private List<Skill> skills;
        private List<Course> courses;
        private List<Intrest> intrests;

        [Key]
        public int Id { get => id; set => id = value; }
        public ExtendedUser User { get => user; set => user = value; }
        public virtual List<Job> Jobs { get => jobs; set => jobs = value; }
        public virtual List<Education> Educations { get => educations; set => educations = value; }
        public virtual List<Language> Languages { get => languages; set => languages = value; }
        public virtual List<Skill> Skills { get => skills; set => skills = value; }
        public virtual List<Course> Courses { get => courses; set => courses = value; }
        public virtual List<Intrest> Intrests { get => intrests; set => intrests = value; }
        [NotMapped]
        public virtual List<LanguageDictionary> LanguagesDictionary { get; set; }

        public CV()
        {
            Jobs = new List<Job>();
            User = new ExtendedUser();
            Educations = new List<Education>();
            Languages = new List<Language>();
            Skills = new List<Skill>();
            Courses = new List<Course>();
            Intrests = new List<Intrest>();
            LanguagesDictionary = new List<LanguageDictionary>();
        }
        public CV(ExtendedUser user)
        {
            User = user;
            Jobs = new List<Job>();
            Educations = new List<Education>();
            Languages = new List<Language>();
            Skills = new List<Skill>();
            Courses = new List<Course>();
            Intrests = new List<Intrest>();
            LanguagesDictionary = new List<LanguageDictionary>();
        }
    }
}