﻿using Recruitment_Support_System.Models.CurriculumVitae.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Models.CurriculumVitae
{
    public class Course : ICvId
    {
        private int id;
        private CV cV;
        private string name;
        private string org;
        private DateTime passed;
        private DateTime validTo;

        [Key]
        public int Id { get => id; set => id = value; }
        public CV CV { get => cV; set => cV = value; }
        [Display(Name = "Nazwa")]
        public string Name { get => name; set => name = value; }
        [Display(Name = "Organizacja")]
        public string Org { get => org; set => org = value; }
        [Display(Name = "Zdany")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Passed { get => passed; set => passed = value; }
        [Display(Name = "Ważny do")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ValidTo { get => validTo; set => validTo = value; }
    }
}