﻿using Recruitment_Support_System.Models.CurriculumVitae.Interfaces;
using Recruitment_Support_System.Models.Dictionaries;
using Recruitment_Support_System.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Models.CurriculumVitae
{
    public class Language : ICvId
    {
        private int id;
        private CV cV;
        private LanguageDictionary languageDictionary;
        private LanguageLevel level;

        [Key]
        public int Id { get => id; set => id = value; }
        public CV CV { get => cV; set => cV = value; }
        public LanguageDictionary LanguageDictionary { get => languageDictionary; set => languageDictionary = value; }
        [Display(Name = "Poziom")]
        public LanguageLevel Level { get => level; set => level = value; }
    }
}