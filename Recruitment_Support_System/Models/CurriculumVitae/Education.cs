﻿using Recruitment_Support_System.Models.CurriculumVitae.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Models.CurriculumVitae
{
    public class Education : ICvId
    {
        private int id;
        private CV cV;
        private string name;
        private string title;
        private DateTime start;
        private DateTime end;

        [Key]
        public int Id { get => id; set => id = value; }
        public CV CV { get => cV; set => cV = value; }
        [Display(Name = "Kierunek")]
        public string Name { get => name; set => name = value; }
        [Display(Name = "Tytuł")]
        public string Title { get => title; set => title = value; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Start { get => start; set => start = value; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime End { get => end; set => end = value; }
    }
}