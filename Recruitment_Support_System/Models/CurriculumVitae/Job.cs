﻿using Recruitment_Support_System.Models.CurriculumVitae.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Models.CurriculumVitae
{
    public class Job : ICvId
    {
        private int id;
        private CV cV;
        private string name;
        private DateTime start;
        private DateTime end; //Min value as current
        private string comment;
        private string company;

        [Key]
        public int Id { get => id; set => id = value; }
        public CV CV { get => cV; set => cV = value; }
        [Display(Name = "Nazwa")]
        public string Name { get => name; set => name = value; }
        [Display(Name = "Data rozpoczęcia")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Start { get => start; set => start = value; }
        [Display(Name = "Data zakończenia")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime End { get => end; set => end = value; }
        [Display(Name = "Komentarz")]
        [DataType(DataType.MultilineText)]
        public string Comment { get => comment; set => comment = value; }
        [Display(Name = "Firma")]
        public string Company { get => company; set => company = value; }
    }
}