﻿using Recruitment_Support_System.Models.CurriculumVitae;
using Recruitment_Support_System.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Models
{
    public class JobOffer
    {
        private int id;
        private Company company;
        private string title;
        private string about;
        private string responsibilities;
        private string requirements;
        private Int16 teamSize;
        private long zarobkiMin;
        private long zarobkiMax;
        private string currency;
        private billingPeriod okresRozliczeniowy;
        private JobLevel jobLevel;
        private JobContract jobContract;
        private string[] skills;
        private bool remote;
        private bool remoteInterview;
        private bool isDeleted;
        private DateTime expirationDate;

        public int Id { get => id; set => id = value; }
        public Company Company { get => company; set => company = value; }
        [Display(Name = "Tytuł")]
        public string Title { get => title; set => title = value; }

        [Display(Name = "Opis")]
        public string About { get => about; set => about = value; }
        [Display(Name = "Obowiązki")]
        public string Responsibilities { get => responsibilities; set => responsibilities = value; } //TODO: split them with something like & or ~.
        [Display(Name = "Wymagania")]
        public string Requirements { get => requirements; set => requirements = value; } //TODO: same here
        [Display(Name = "Wielkość zespołu")]
        public short TeamSize { get => teamSize; set => teamSize = value; }
        [Display(Name = "Poziom stanowiska")]
        public JobLevel JobLevel { get => jobLevel; set => jobLevel = value; }
        [Display(Name = "Rodzaj umowy")]
        public JobContract JobContract { get => jobContract; set => jobContract = value; }
        public string[] Skills { get => skills; set => skills = value; }
        [Display(Name = "Wymagane umiejętności", Description = "Wpisz wymagane umiejętności odzielone spacją")]
        public string Skill
        {
            get => skills == null ? "" : string.Join(" ", skills);
            set => skills = value.Split(' ');
        }
        public ICollection<Application> Applications { get; set; }
        [Display(Name = "Minimalne zarobki")]
        public long ZarobkiMin { get => zarobkiMin; set => zarobkiMin = value; }
        [Display(Name = "Maxymalne zarobki")]
        public long ZarobkiMax { get => zarobkiMax; set => zarobkiMax = value; }
        [Display(Name = "Okres  rozliczeniowy")]
        public billingPeriod OkresRozliczeniowy { get => okresRozliczeniowy; set => okresRozliczeniowy = value; }
        [Display(Name = "Waluta")]
        public string Currency { get => currency; set => currency = value; }
        [Display(Name = "Praca zdalna")]
        public bool Remote { get => remote; set => remote = value; }
        [Display(Name = "Zdalna rozmowa rekrutacyjna")]
        public bool RemoteInterview { get => remoteInterview; set => remoteInterview = value; }
        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }
        [Display(Name = "Data wygaśnięcia")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ExpirationDate { get => expirationDate; set => expirationDate = value; }
    }

    public class Application
    {
        [Key, Column(Order = 0)]
        [ForeignKey("ExtendedUser") ]
        public string ExtendedUser_Id { get; set; }
        [Key, Column(Order = 1)]
        [ForeignKey("JobOffer")]
        public int JobOffer_Id { get; set; }

        public virtual ExtendedUser ExtendedUser { get; set; }
        public virtual JobOffer JobOffer { get; set; }

        public bool GotInterview { get; set; }
        public bool GotJobOffer { get; set; }
    }
}