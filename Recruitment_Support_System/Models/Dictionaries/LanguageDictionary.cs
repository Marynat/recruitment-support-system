﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Models.Dictionaries
{
    public class LanguageDictionary
    {
        private int id;
        private string name;
        private string countryCode;

        [Key]
        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string CountryCode { get => countryCode; set => countryCode = value; }  

        //TODO: Fill dictionaries with Data
    }
}