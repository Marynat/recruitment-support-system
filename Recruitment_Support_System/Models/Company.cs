﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Models
{
    public class Company
    {
        private int id;
        private string name;
        private string city;
        private string address;
        private int size;
        private List<JobOffer> jobOffers;
        private ExtendedUser owner;

        [Key]
        public int Id { get => id; set => id = value; }
        [Display(Name = "Nazwa")]
        public string Name { get => name; set => name = value; }
        [Display(Name = "Miasto")]
        public string City { get => city; set => city = value; }
        [Display(Name = "Adress")]
        public string Address { get => address; set => address = value; }
        [Display(Name = "Wielkość zespołu")]
        public int Size { get => size; set => size = value; }
        public List<JobOffer> JobOffers { get => jobOffers; set => jobOffers = value; }
        public ExtendedUser Owner { get => owner; set => owner = value; }
    }
}