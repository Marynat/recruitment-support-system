﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Models.Enums
{
    public enum SkillLevel
    {
        Początkujący,
        Podstawowy,
        Średniozaawansowany,
        Zaawansowany,
        Biegły
    }

    public enum LanguageLevel
    {
        Początkujący,
        Podstawowy,
        Średniozaawansowany,
        Zaawansowany,
        Biegły,
        Ojczysty
    }

    public enum JobLevel
    {
        stażysta,
        junior,
        mid,
        senior,
        expert
    }

    public enum JobContract
    {
        UOP,
        B2B,
        Zlecenie,
        Dzieło
    }

    public enum AccountType
    {
        prywatne,
        firmowe
    }

    public enum billingPeriod
    {
        godzinowy,
        dzienny,
        tygodniowy,
        miesięczny,
        roczny
    }
    enum UserSchool
    {
        Technik,
        Inżynier,
        Magister,
        Dr
    }
}