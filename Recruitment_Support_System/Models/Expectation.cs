﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Models
{
    public class Expectation
    {
        private int id;
        private ExtendedUser extendedUser;
        private int salaryFrom;
        private int salaryTo;
        private bool rWork;
        private bool rRecruitment;

        [Key]
        public int Id { get => id; set => id = value; }
        public ExtendedUser ExtendedUser { get => extendedUser; set => extendedUser = value; }
        public int SalaryFrom { get => salaryFrom; set => salaryFrom = value; }
        public int SalaryTo { get => salaryTo; set => salaryTo = value; }
        public bool RWork { get => rWork; set => rWork = value; }
        public bool RRecruitment { get => rRecruitment; set => rRecruitment = value; }

    }
}