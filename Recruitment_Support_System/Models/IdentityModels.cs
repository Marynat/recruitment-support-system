﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Recruitment_Support_System.Models.CurriculumVitae;
using Recruitment_Support_System.Models.Dictionaries;

namespace Recruitment_Support_System.Models
{
    public class ExtendedUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ExtendedUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
        [Display(Name ="Nazwa użytkownika")]
        public override string UserName { get => base.UserName; set => base.UserName = value; }

        [Display(Name="Numer telefonu")]
        public override string PhoneNumber { get => base.PhoneNumber; set => base.PhoneNumber = value; }
        [Display(Name = "Imie")]
        public string Name { get; set; }
        [Display(Name = "Nazwisko")]
        public string Surname { get; set; }
        [Display(Name = "Wiek")]
        public int Age { get; set; }
        [Display(Name = "Data urodzenia")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Birthdate { get; set; }
        [NotMapped]
        public virtual Expectation Expectation { get; set; }

        public virtual ICollection<Application> Applications { get; set; }
        [NotMapped]
        public virtual CV CVs { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ExtendedUser>
    {
        public DbSet<Expectation> Expectations { get; set; }
        public DbSet<CV> CVs { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Education> Educations { get; set; }
        public DbSet<Intrest> Interests { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<LanguageDictionary> LanguageDictionary { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<JobOffer> JobOffers { get; set; }
        public DbSet<Application> ExtendedUserJobOffers { get; set; }

        public ApplicationDbContext()
            : base(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\OEM\source\repos\Recruitment_Support_System\Recruitment_Support_System\rssDB.mdf;Integrated Security=True;Connect Timeout=30", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}