﻿using Recruitment_Support_System.Models;
using Recruitment_Support_System.Models.CurriculumVitae;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Helpers
{
    public class UpdateHelper
    {
        public static void UpdateJob(Job job, Job updatedJob)
        {
            job.Name = updatedJob.Name;
            job.Start = updatedJob.Start;
            job.Comment = updatedJob.Comment;
            job.Company = updatedJob.Company;
            job.End = updatedJob.End;
        }

        public static void UpdateEducation(Education edu, Education updatedEdu)
        {
            edu.Name = updatedEdu.Name;
            edu.Start = updatedEdu.Start;
            edu.Title = updatedEdu.Title;
            edu.End = updatedEdu.End;
        }

        public static void UpdateCourse(Course course, Course updatedCourse)
        {
            course.Name = updatedCourse.Name;
            course.Org = updatedCourse.Org;
            course.Passed = updatedCourse.Passed;
            course.ValidTo = updatedCourse.ValidTo;
        }

        public static void UpdateSkill(Skill skill, Skill updatedSkill)
        {
            skill.Level = updatedSkill.Level;
            skill.Name = updatedSkill.Name;
        }

        public static void UpdateCompany(Company company, Company updatedCompany)
        {
            company.Address = updatedCompany.Address;
            company.City = updatedCompany.City;
            company.Name = updatedCompany.Name;
            company.Size = updatedCompany.Size;
        }
        public static void UpdateJobOffer(JobOffer offer, JobOffer uOffer)
        {
            offer.JobContract = uOffer.JobContract;
            offer.JobLevel = uOffer.JobLevel;
            offer.OkresRozliczeniowy = uOffer.OkresRozliczeniowy;
            offer.Remote = uOffer.Remote;
            offer.RemoteInterview = uOffer.RemoteInterview;
            offer.Requirements = uOffer.Requirements;
            offer.Responsibilities = uOffer.Responsibilities;
            offer.Skill = uOffer.Skill;
            offer.TeamSize = uOffer.TeamSize;
            offer.Title = uOffer.Title;
            offer.ZarobkiMax = uOffer.ZarobkiMax;
            offer.ZarobkiMin = uOffer.ZarobkiMin;
            offer.About = uOffer.About;
            offer.Currency = uOffer.Currency;
            offer.IsDeleted = uOffer.IsDeleted;
        }

        public static void UpdateUser(ExtendedUser ex, ExtendedUser uEx)
        {
            ex.Name = uEx.Name;
            ex.PhoneNumber = uEx.PhoneNumber;
            ex.Surname = uEx.Surname;
            ex.Email = uEx.Email;
            ex.UserName = uEx.UserName;
            ex.Birthdate = uEx.Birthdate;
            ex.Age = uEx.Age;
        }
    }
}