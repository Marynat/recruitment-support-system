﻿using Recruitment_Support_System.Models;
using Recruitment_Support_System.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Helpers
{
    public class AttributeHelper
    {
        private static List<string> keys = new List<string> { "java", "php", "csharp", ".net" };
        private static List<string> listOfRelatedSpecialization = new List<string>() { "informatyka", "psychologia i informatyka", "automatyka i informatyka przemysłowa", "automatyka", "cyberbezpieczeństwo", "ui/ux designer", "grafika komputerowa", "inżynieria danych", "teleinformatyka", "informatyka algorytmiczna" };
        private static double maxWiekPr = 18.6;
        private static Random r = new Random(1);
        public static double[] getSingleRowWithOut(Application a)
        {
            using (var db = new ApplicationDbContext())
            {
                var attr = getMainAttributes(a);

                return new double[] { attr[0], attr[1], attr[2], attr[3], a.GotJobOffer ? 0.0 : a.GotInterview ? 1 : 0, a.GotJobOffer ? 0.0 : !a.GotInterview ? 1 : 0, a.GotJobOffer ? 1.0 : 0.0 };
            }
        }
        public static double[] getSingleRow(Application a)
        {
            return getMainAttributes(a);
        }

        private static double[] getMainAttributes(Application a)
        {
            using (var db = new ApplicationDbContext())
            {
                var cv = db.CVs.Include("Jobs").Include("Courses").Include("Educations").FirstOrDefault(c => c.User.Id == a.ExtendedUser_Id);
                string key = keys.FirstOrDefault(k => cv.Jobs.Any(j => j.Name.Contains(k)));
                key = key ?? "None";
                string spec = listOfRelatedSpecialization.Where(s => cv.Educations.Any(e => e.Name.Contains(s))).FirstOrDefault();
                spec = spec ?? "NotRelated";
                double firstAttribute = 2.1;
                double secondAttribute = -3.2;
                double thirdttribute = -4.1;
                double fourhtAttribute = 1.7;
                if (cv.Jobs.Count > 0)
                {
                    firstAttribute = cv.Jobs.Any(j => j.Name.ToLower().Contains(key) && a.JobOffer.Title.ToLower().Contains(key)) ? 1.42 : -1.67;
                    secondAttribute = (cv.Jobs.Sum(j => j.End.Year - j.Start.Year) + 1) / maxWiekPr;
                }
                if (cv.Courses.Count > 0)
                    thirdttribute = (cv.Courses.Where(c => c.Name.Contains(key)).Count() + 1) / 1.74;
                if (cv.Educations.Count > 0)
                    fourhtAttribute = (int)a.JobOffer.JobLevel - 6.3;

                return new double[] { firstAttribute, secondAttribute, thirdttribute, fourhtAttribute };
            }
        }

        public static double addOutputValues(double[] o)
        {
            //this is for my specyfic case.
            int firstOutValue = 0;
            int thirdOutValue = 2;

            double ret = o[firstOutValue] + (o[thirdOutValue] * 3) * 100;
            return ret;
        }
    }
}