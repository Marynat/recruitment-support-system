﻿using Recruitment_Support_System.Models.CurriculumVitae.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recruitment_Support_System.Helpers
{
    public class ComparerHelper : IEqualityComparer<ICvId>
    {
        public int GetHashCode(ICvId co)
        {
            if (co == null)
            {
                return 0;
            }
            return co.Id.GetHashCode();
        }

        public bool Equals(ICvId x1, ICvId x2)
        {
            if (object.ReferenceEquals(x1, x2))
            {
                return true;
            }
            if (object.ReferenceEquals(x1, null) ||
                object.ReferenceEquals(x2, null))
            {
                return false;
            }
            return x1.Id == x2.Id;
        }
    }
}