﻿using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Recruitment_Support_System.Controllers;
using Recruitment_Support_System.Models;
using Recruitment_Support_System.Models.CurriculumVitae;
using Recruitment_Support_System.Models.Dictionaries;
using Recruitment_Support_System.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Recruitment_Support_System.Tests.Controllers
{
    [TestClass]
    public class CvConTest
    {

        string userId = "da75ba13-8eee-451b-ae8b-78779363f141";

        private Claim claim;
        private ClaimsIdentity mockIden;
        private MockHttpSession session;
        private ControllerContext context;
        private ExtendedUser user;
        private Mock<IUserStore<ExtendedUser>> store;
        private Mock<ApplicationUserManager> mockUm;
        private CvController c;

        [TestInitialize]
        public void Init()
        {
            claim = new Claim("test", userId);
            mockIden = Mock.Of<ClaimsIdentity>(ci => ci.FindFirst(It.IsAny<string>()) == claim);
            session = new MockHttpSession();
            session["cV"] = null;
            context = Mock.Of<ControllerContext>(cc => cc.HttpContext.User == mockIden && cc.HttpContext.Session == session);


            using (var db = new ApplicationDbContext())
            {
                user = db.Users.FirstOrDefault(u => u.Id == userId);

                store = new Mock<IUserStore<ExtendedUser>>();
                store.As<IUserStore<ExtendedUser>>().Setup(x => x.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(user);

                mockUm = new Mock<ApplicationUserManager>(store.Object);
                mockUm.Setup(a => a.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(user);

                c = new CvController(mockUm.Object)
                {
                    ControllerContext = context
                };

                c.GetUserId = () => userId;
            }
        }

        [TestMethod]
        public async Task TestIndex()
        {


            ViewResult r = await c.Index() as ViewResult;

            CV model = (CV)r.Model;

            Assert.IsNotNull(r);

            Assert.AreEqual("jatyon14@wp.pl", model.User.Email);

        }
        [TestMethod]
        public async Task TestJob()
        {
            ViewResult r = await c.Index() as ViewResult;

            var a = c.Job(new Job() { Name = "TestJob", Start = DateTime.Now, End = DateTime.Now, Company = "Best", Comment = "Best" }) as RedirectResult;



            CV model = (CV)r.Model;

            Assert.IsNotNull(r);

            Assert.AreEqual("jatyon14@wp.pl", model.User.Email);
            Assert.IsNotNull(model.Jobs);
            Assert.AreEqual(1, model.Jobs.Count);

        }
        [TestMethod]
        public async Task TestIntrest()
        {
            ViewResult r = await c.Index() as ViewResult;

            var a = c.Intrest(new Intrest() { Name = "Test" }) as ViewResult;

            CV model = (CV)r.Model;

            Assert.IsNotNull(r);

            Assert.AreEqual("jatyon14@wp.pl", model.User.Email);

        }
        [TestMethod]
        public async Task TestSkill()
        {
            ViewResult r = await c.Index() as ViewResult;

            var a = c.Skill(new Skill() { Name = "TestSkill", Level = Models.Enums.SkillLevel.Zaawansowany }) as ViewResult;

            CV model = (CV)r.Model;

            Assert.IsNotNull(r);

            Assert.AreEqual("jatyon14@wp.pl", model.User.Email);

        }

        [TestMethod]
        public async Task TestCourse()
        {
            ViewResult r = await c.Index() as ViewResult;

            var a = c.Course(new Course() { Name = "TestCourse", Org = "TestOrg", Passed = DateTime.Now, ValidTo = DateTime.Now.AddYears(5) }) as ViewResult;

            CV model = (CV)r.Model;

            Assert.IsNotNull(r);

            Assert.AreEqual("jatyon14@wp.pl", model.User.Email);

        }
        [TestMethod]
        public async Task TestEducation()
        {
            ViewResult r = await c.Index() as ViewResult;

            var a = c.Education(new Education() { Name = "TestEducation", End = DateTime.Now, Start = DateTime.Now.AddYears(-5), Title = "TestEd" }) as ViewResult;

            CV model = (CV)r.Model;

            Assert.IsNotNull(r);

            Assert.AreEqual("jatyon14@wp.pl", model.User.Email);

        }










        public class MockHttpSession : HttpSessionStateBase
        {
            Dictionary<string, object> m_SessionStorage = new Dictionary<string, object>();

            public override object this[string name]
            {
                get { return m_SessionStorage[name]; }
                set { m_SessionStorage[name] = value; }
            }
        }
    }
}
