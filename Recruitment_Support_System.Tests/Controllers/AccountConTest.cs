﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Recruitment_Support_System.Controllers;
using Recruitment_Support_System.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Recruitment_Support_System.Tests.Controllers
{
    /// <summary>
    /// Summary description for AccountConTest
    /// </summary>
    [TestClass]
    public class AccountConTest
    {

        [TestMethod]
        public async Task TestLogin()
        {
            AccountController ac = new AccountController();

            var lvm = new LoginViewModel() { Email = "jatyon14@wp.pl", Password = "Init123$", RememberMe = false };

            ViewResult r = await ac.Login(lvm, "home") as ViewResult;

            Console.WriteLine(r.ToString());

            Assert.IsNotNull(r);

        }
    }
}
