﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Recruitment_Support_System;
using Recruitment_Support_System.Controllers;
using Recruitment_Support_System.Models;
using Recruitment_Support_System.Models.Builders;
using Recruitment_Support_System.Models.Builders.CvBuilders;
using Recruitment_Support_System.Models.CurriculumVitae;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Recruitment_Support_System.Tests.Controllers
{
    [TestClass]
    public class HomeConTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.AreEqual("Opis projektu.", result.ViewBag.Message);
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void JobOffers()
        {
            HomeController home = new HomeController();

            ViewResult r = home.JobOffers() as ViewResult;

            var model = (IEnumerable<JobOffer>)r.Model;

            Assert.AreEqual(30, model.Count());
        }

        //[TestMethod]
        //public void testExcept()
        //{
        //    ICVBuilder cV = new DatabseCVBuilder(new CV() { Id = 10, Courses = new List<Course>() { new Course() { Id = 3, Name = "jakis kurs", Org = "super", Passed = DateTime.Today.AddMonths(-20), ValidTo = DateTime.Today.AddMonths(20) } } });
        //    cV.BuildCourses();
        //}
    }
}
