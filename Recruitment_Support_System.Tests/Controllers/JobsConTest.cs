﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Recruitment_Support_System.Controllers;
using System.Web.Mvc;
using System.Web;
using System.Security.Principal;
using System.Collections;
using Recruitment_Support_System.Models;
using System.Collections.Generic;
using System.Linq;
using Moq;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace Recruitment_Support_System.Tests.Controllers
{
    [TestClass]
    public class JobsConTest
    {
        string userId = "60ea9640-08ac-4572-829e-e0663a1aeda7";

        private Claim claim;
        private ClaimsIdentity mockIden;
        private ControllerContext context;
        private ExtendedUser user;
        private Mock<IUserStore<ExtendedUser>> store;
        private Mock<ApplicationUserManager> mockUm;
        private JobsController c;

        [TestInitialize]
        public void Init()
        {
            claim = new Claim("test", userId);
            mockIden = Mock.Of<ClaimsIdentity>(ci => ci.FindFirst(It.IsAny<string>()) == claim);
            context = Mock.Of<ControllerContext>(cc => cc.HttpContext.User == mockIden);

            using (var db = new ApplicationDbContext())
            {
                user = db.Users.FirstOrDefault(u => u.Id == userId);

                store = new Mock<IUserStore<ExtendedUser>>();
                store.As<IUserStore<ExtendedUser>>().Setup(x => x.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(user);

                mockUm = new Mock<ApplicationUserManager>(store.Object);
                mockUm.Setup(a => a.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(user);

                c = new JobsController(mockUm.Object)
                {
                    ControllerContext = context
                };

                c.GetUserId = () => userId;
            }
        }

        [TestMethod]
        public async Task TestIndex()
        {


            ViewResult r = await c.Index() as ViewResult;

            IEnumerable<JobOffer> model = (IEnumerable<JobOffer>)r.Model;

            Assert.AreEqual(5, model.Count());

        }

        [TestMethod]
        public async Task TestJobOffer()
        {
            ViewResult r = c.JobOffer(66, "home+test") as ViewResult;

            Assert.IsNotNull(r);

            Assert.ThrowsException<HttpException>(() => r = c.JobOffer(999, "Home+test") as ViewResult);
        }

        [TestMethod]
        public async Task TestJobOffersExceptions()
        {
            try
            {
                ViewResult r = c.JobOffer(999, "Home+test") as ViewResult;
            }
            catch (HttpException e)
            {
                Assert.AreEqual(404, e.GetHttpCode());
            }
        }

        [TestMethod]
        public async Task TestJobOffersExceptions2()
        {
            ViewResult r;
            Assert.ThrowsException<IndexOutOfRangeException>(() => r = c.JobOffer(30, "Thiswillthrow") as ViewResult);
        }
    }
}
